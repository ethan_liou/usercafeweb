import google_form.doc_manager
import memcache
import pymongo
from bson import ObjectId
import answer_obj
import mongo_wrapper

MEMCACHE_SERVER='usercafecache.4xykkg.cfg.usw1.cache.amazonaws.com:11211'

KEY_ADS = "ad_list"

def del_ad(survey_id):
    db_ad_metadata = mongo_wrapper.get_instance().usercafe_ad.ad_metadata  
    try:
        # if no metadata, create it
        db_ad_metadata.remove({'survey_id':survey_id})
        mc = memcache.Client([MEMCACHE_SERVER],debug=0)
        mc.delete(KEY_ADS)
    except:
        pass

def add_ad(data):
    db_ad_metadata = mongo_wrapper.get_instance().usercafe_ad.ad_metadata  
    try:
        # if no metadata, create it
        db_ad_metadata.update(
            {'survey_id':data['survey_id']},
            {'$set':data},
            upsert=True)
        mc = memcache.Client([MEMCACHE_SERVER],debug=0)
        mc.delete(KEY_ADS)
    except:
        pass
    return data

def change_ad(survey_id,data):
    db_ad_metadata = mongo_wrapper.get_instance().usercafe_ad.ad_metadata
    data_to_set = {}
    data_to_set['banner'] = data['banner']
    data_to_set['enabled'] = data['enabled']
    db_ad_metadata.update({'survey_id':survey_id},{'$set':data_to_set})
    mc = memcache.Client([MEMCACHE_SERVER],debug=0)
    mc.delete(KEY_ADS)    

def add_ad_view_cnt(form_key):
    db_ad_metadata = mongo_wrapper.get_instance().usercafe_ad.ad_metadata
    db_ad_metadata.update({'form_key':form_key},{'$inc':{'view_cnt':1}})

def add_ad_response_cnt(form_key):
    db_ad_metadata = mongo_wrapper.get_instance().usercafe_ad.ad_metadata
    db_ad_metadata.update({'form_key':form_key},{'$inc':{'response_cnt':1}})

def list_ad_metas(load_cache=True):
    ads = None
    if load_cache:
        mc = memcache.Client([MEMCACHE_SERVER],debug=0)
        ads = mc.get(KEY_ADS)
    if not ads:
        db_ad_metadata = mongo_wrapper.get_instance().usercafe_ad.ad_metadata
        metadatas = db_ad_metadata.find()
        meta_arr = []
        for metadata in metadatas:
            del metadata['_id']
            answer_cnt = answer_obj.get_answer_cnt(metadata['survey_id'])
            metadata['response_cnt'] = answer_cnt
            meta_arr.append(metadata)
        if load_cache:
            mc.set(KEY_ADS,str(meta_arr))
        return meta_arr
    else:
        import ast
        return ast.literal_eval(ads)

# return [{id,title,form_key}], search order : memcached, db
def list_ads():
    # from memcached
    mc = memcache.Client([MEMCACHE_SERVER],debug=0)
    ads = mc.get(KEY_ADS)
    if not ads:
        # from db
        db_ads = mongo_wrapper.get_instance().usercafe_ad.ads
        ads = db_ads.find()
        ads_arr = []
        for ad in ads:
            ads_arr.append({'id':str(ad['form_key']),'title':ad['title']})
        mc.set(KEY_ADS,str(ads_arr))
        return ads_arr
    else:
        import ast
        return ast.literal_eval(ads)