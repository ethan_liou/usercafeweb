import pymongo
import user_obj
import developer_obj
import ad_obj

KEY_DEVELOPER = '_developer_id'
KEY_DEVICE_ID = '_device_id'
KEY_TIMESTAMP = '_timestamp'

def get_answer_cnt(survey_id):
    db_answers = mongo_wrapper.get_instance().usercafe.answers    
    cur = db_answers.find({'_survey_id':survey_id,'_finished':'true'})
    if cur is None:
        return 0
    return cur.count()

def list_answer(survey_id):
    db_answers = mongo_wrapper.get_instance().usercafe.answers    
    answer_arr = []
    answers = db_answers.find({'_survey_id':survey_id},fields={
        '_id':False,
        '_submit':False,
        '_account':False,
        '_developer_id':False,
        '_device_id':False,
        '_form_key':False,
        '_dig':False
    })
    for answer in answers:
        if '_timestamp' in answer.keys():
            answer[KEY_TIMESTAMP] = answer['_timestamp'].split('.')[0]
        answer_arr.append(answer)
    return answer_arr

def insert_answer(device_id,developer_id,ad_id,answer_dict):
    answer_dict[KEY_DEVELOPER] = developer_id
    answer_dict[KEY_DEVICE_ID] = device_id
    import datetime
    answer_dict[KEY_TIMESTAMP] = datetime.datetime.now().isoformat()
    db_answers = mongo_wrapper.get_instance().usercafe_ad.answers    
    db_answers.insert(answer_dict)
    ad_obj.add_ad_response_cnt(ad_id)
    developer_obj.add_response_cnt(developer_id)
    user_obj.add_finished_ad(device_id,ad_id)