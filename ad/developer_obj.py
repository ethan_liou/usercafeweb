import pymongo
from bson import ObjectId
import mongo_wrapper

def register_user(email):
    db_developers = mongo_wrapper.get_instance().usercafe_ad.developers    
    db_developers.insert({'email':email,'click':0,'response':0,'view':0})    

def get_stat(dev_id):
    db_developers = mongo_wrapper.get_instance().usercafe_ad.developers    
    data = db_developers.find_one({'_id':ObjectId(dev_id)})
    if data:
        return data['view'],data['click'],data['response']
    return None

def add_view_cnt(dev_id):
    db_developers = mongo_wrapper.get_instance().usercafe_ad.developers    
    db_developers.update({'_id':ObjectId(dev_id)},{'$inc':{'view':1}})

def add_click_cnt(dev_id):
    db_developers = mongo_wrapper.get_instance().usercafe_ad.developers    
    db_developers.update({'_id':ObjectId(dev_id)},{'$inc':{'click':1}})

def add_response_cnt(dev_id):
    db_developers = mongo_wrapper.get_instance().usercafe_ad.developers    
    db_developers.update({'_id':ObjectId(dev_id)},{'$inc':{'response':1}})    