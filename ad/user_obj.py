import pymongo
import memcache
from bson import ObjectId

MEMCACHE_SERVER='usercafecache.4xykkg.cfg.usw1.cache.amazonaws.com:11211'
MEMCACHED_USER_PREFIX="user_"

def add_user(device_id,finished_ads):
    db_users = mongo_wrapper.get_instance().usercafe_ad.users    
    db_users.insert({
        '_id':device_id,
        'finished_ads':finished_ad
    })

def add_finished_ad(device_id,ad_id):
    device_id = str(device_id)
    # update memcached
    mc = memcache.Client([MEMCACHE_SERVER],debug=0)
    ads = mc.delete(MEMCACHED_USER_PREFIX+device_id)
    
    # update db
    db_users = mongo_wrapper.get_instance().usercafe_ad.users
    db_users.update({ 'device_id': device_id }, { '$push': { 'finished_ad': ad_id }} , upsert=True)
    
def get_finished_ad(device_id):
    device_id = str(device_id)
    mc = memcache.Client([MEMCACHE_SERVER],debug=0)
    ads = mc.get(MEMCACHED_USER_PREFIX+device_id)
    if not ads:
        db_answers = mongo_wrapper.get_instance().usercafe.answers            
        cur = db_answers.find({'_device_id':device_id})
        arr = []
        for c in cur:
            if '_survey_id' in c.keys() and c['_survey_id'] not in arr:
                if '_finished' in c.keys() and c['_finished'] == 'false':
                    continue
                arr.append(c['_survey_id'])
        mc.set(MEMCACHED_USER_PREFIX+device_id,','.join(arr))
        return arr
    return ads.split(',')
        
        