#!/usr/bin/python
# -*- coding: utf8 -*-
import os
import json
import hashlib
try:
    import uwsgi
    support_uwsgi = True
except:
    support_uwsgi = False
    pass
import logging
from flask import Flask,request,Response,abort,render_template,make_response,escape,redirect,url_for,session,flash,jsonify

import utils

import ad.ad_obj as ad_obj
import ad.developer_obj as dev_obj
import ad.user_obj as user_obj
import auth_helper

import random
random.seed(os.urandom(10))

MEMCACHE_SERVER='usercafecache.4xykkg.cfg.usw1.cache.amazonaws.com:11211'

logger = logging.getLogger(__name__)

application = Flask(__name__)
application.secret_key = 'osadvkubweiubvuwebvuiw'
application.debug = True
application.template_folder = 'web'

AD_APP_ID = '520195602a4f5641c87921ed'
DEFAULT_DEV_ID = '5201222951faa63812006f4b'

@application.route('/ad/')
@application.route('/ad/index.html')
def index():
    user = auth_helper.get_user(session)
    if user is None:
        return render_template('ad/index.html')
    elif auth_helper.is_ad_valid_user(user):
        return render_template('ad/index.html',email=user.user_data['email'],valid=True)        
    else:
        return render_template('ad/index.html',email=user.user_data['email'],valid=False)

@application.route('/adapi/ads',methods=['GET'])
def get_ad_list():
    ads = ad_obj.list_ad_metas(False)
    return Response(json.dumps(ads),  mimetype='application/json')

@application.route('/adapi/ads',methods=['PUT'])
def upload_ad():
    data = request.json
    if 'url' in data:
        # call web func
        try:
            import urllib
            encoded_data = urllib.urlencode(data)
            http_url = 'http://localhost/api/app/{0}/survey/googledoc'.format(AD_APP_ID)
            survey_data = utils.send_request(http_url,'POST',request.cookies,encoded_data)
            import ast
            data_dict = ast.literal_eval(survey_data)
        except:
            return make_response("",400)
        ad_meta = {
            'survey_id':str(data_dict['_id']),
            'title':data_dict['title'].decode('unicode-escape').encode('utf-8'),
            'url':data_dict['url'],
        }
        ad_obj.add_ad(ad_meta)
        return jsonify(**ad_meta)
    return make_response("",400)

@application.route('/adapi/ads/<survey_id>',methods=['POST'])
def update_ad(survey_id):
    data = request.json
    ad_obj.change_ad(survey_id,data)
    return make_response("",200)    

@application.route('/adapi/ads/<survey_id>',methods=['DELETE'])
def delete_ad(survey_id):
    # call web func
    try:
        http_url = 'http://localhost/api/app/{0}/survey/{1}'.format(AD_APP_ID,survey_id)
        survey_data = utils.send_request(http_url,'DELETE',request.cookies,None)
    except Exception, e:
        logging.error(str(e))
        return make_response("",400)    
    if survey_data != None:
        ad_obj.del_ad(survey_id)
    return make_response("",200)

@application.route('/adapi/developer/register',methods=['POST'])
def register_developer():
    data = request.json
    if data is not None and 'email' in data:
        dev_obj.register_user(data['email'])
        return make_response("",200)
    return make_response("",400)

@application.route('/adapi/ads/user/<device_id>',methods=['GET'])
def get_unfinished_ad(device_id):
    ads = ad_obj.list_ad_metas()
    finished_ads = user_obj.get_finished_ad(device_id)
    #finished_ads = [] # can repeat
    unfinished_ads = []
    for ad in ads:
        if 'survey_id' in ad.keys() and ad['survey_id'] not in finished_ads and ad['enabled'] == True:
            banner = ad['banner']
            if len(banner) != 0:
                banner_idx = random.randint(0,len(banner)-1)
                ad['banner'] = banner[banner_idx]
                ad['banner_idx'] = banner_idx
            else:
                del ad['banner']
            unfinished_ads.append(ad)
    if len(unfinished_ads) == 0:
        return make_response("",204)
    dev_obj.add_view_cnt(DEFAULT_DEV_ID)
    return Response(json.dumps(unfinished_ads[random.randint(0,len(unfinished_ads)-1)]),  mimetype='application/json')

if __name__ == '__main__':
    application.run(host="0.0.0.0",port=8081)