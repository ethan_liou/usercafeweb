import pymongo
import logging
from bson import ObjectId
import survey_object
import google_form.entry_items as google_entry
import mongo_wrapper

KEY_DEVELOPER = '_developer_id'
KEY_DEVICE_ID = '_device_id'
KEY_TIMESTAMP = '_timestamp'

SURVEY_AD = '520463aed06500e76763ba07'
def check_repeat(survey_id,device_id):
    db_answers = mongo_wrapper.get_instance().usercafe.answers    
    answer_cnt = db_answers.find({'_survey_id':survey_id,KEY_DEVICE_ID:device_id,'_finished':'true'}).count()
    if survey_id == '5215c8e5d06500e76763ba33' and answer_cnt == 0:
        # check ad
        answer_cnt = db_answers.find({'_survey_id':SURVEY_AD,KEY_DEVICE_ID:device_id,'_finished':'true'}).count()
    return answer_cnt > 0

def get_answer_stat(survey_id):
    all_questions = survey_object.get_questions_by_survey_id(survey_id)
    countable_questions = []
    for question in all_questions:
        question['answer'] = {}
        if question['type'] == google_entry.TYPE_CHECKBOX or \
           question['type'] == google_entry.TYPE_LIST or \
           question['type'] == google_entry.TYPE_MULTIPLECHOICE or \
           question['type'] == google_entry.TYPE_SCALE:
            countable_questions.append(question)
        elif question['type'] == google_entry.TYPE_GRID:
            keys = question['name'].split('&')
            row_labels = question['row_labels']
            num_key = len(keys)
            for idx in range(num_key):
                q = {'name':keys[idx], 
                     'title':u'{0} - {1}'.format(question['title'],row_labels[idx]), 
                     'type' : google_entry.TYPE_MULTIPLECHOICE,
                     'answer' : {}}
                countable_questions.append(q)
    db_answers = mongo_wrapper.get_instance().usercafe.answers    
    answer_arr = []
    answers = db_answers.find({'_survey_id':survey_id,'$or':[{'_finished':{'$exists':False}},{'_finished':'true'}] },fields={
        '_id':False,
        '_submit':False,
        '_account':False,
        '_developer_id':False,
        '_form_key':False,
        '_dig':False,
        '_timestamp':False
    })
    for answer in answers:
        for question in countable_questions:
            if question['name'] in answer.keys():
                if answer[question['name']] in question['answer'].keys():
                    question['answer'][answer[question['name']]] += 1
                else:
                    question['answer'][answer[question['name']]] = 1
    for k in question['answer'].keys():
        k_striped = k.strip()
        if k != k_striped:
            question['answer'][k_striped] = question['answer'][k]
            del question['answer'][k]
    q2 = []
    for question in countable_questions:
        q2.append({'title':question['title'].strip(),'type':question['type'],'answer':question['answer']})
    return q2

def get_answer_titles(survey_id):
    db_ads = mongo_wrapper.get_instance().usercafe.surveys
    db_answer_metas = mongo_wrapper.get_instance().usercafe.answer_metas
    metas = db_answer_metas.find_one({'survey_id':survey_id})
    ad = db_ads.find_one({'_id':ObjectId(survey_id)})   
    IGNORE_LIST = ['PageBreaker','SectionHeader','_dig','_form_key','_startpage','_finished','_form_key']
    arr = []  
    page_nums = ad['pages'].keys()
    page_nums.sort()
    for page_num in page_nums:
        page = ad['pages'][page_num]
        for question in page['questions']:
            if question['type'] == google_entry.TYPE_GRID:
                keys = question['name'].split('&')
                row_labels = question['row_labels']
                num_key = len(keys)
                for idx in range(num_key):
                    q = {'field':keys[idx].replace('.','_'), 
                         'displayName':u'{0} - {1}'.format(question['title'],row_labels[idx])}
                    arr.append(q)            
            elif 'name' in question and 'title' in question and question['name'] not in IGNORE_LIST and not question['name'].endswith('_t'):
                arr.append({
                    'field':question['name'].replace('.','_'),
                    'displayName':question['title']
                })
    if metas is not None:
        for meta in metas['metas']:
            if meta not in IGNORE_LIST and not meta.endswith('_t'):
                d = {
                    'field':meta,
                    'displayName':meta.strip('_')
                }         
                if meta == '_timestamp':
                    arr.insert(0,d)
                else:
                    arr.append(d)
     
    return arr

def list_answer(survey_id,limit=0):
    db_answers = mongo_wrapper.get_instance().usercafe.answers    
    answer_arr = []
    answers = db_answers.find({'_survey_id':survey_id,'$or':[{'_finished':{'$exists':False}},{'_finished':'true'}] },fields={
        '_id':False,
        '_submit':False,
        '_account':False,
        '_developer_id':False,
        '_form_key':False,
        '_dig':False
    },limit=limit,sort=[('_timestamp',pymongo.DESCENDING)])
    for answer in answers:
        if '_finished' in answer and answer['_finished'] == 'false':
            continue
        try:
            if '_timestamp' in answer.keys():
                answer[KEY_TIMESTAMP] = answer['_timestamp'].split('.')[0]
        except:
            pass
        answer_arr.append(answer)
    return answer_arr

def insert_meta(data):
    IGNORE_KEYS = ['_survey_id','_submit','_app_id']
    db_answer = mongo_wrapper.get_instance().usercafe.answer_metas    
    keys = []
    for key in data.keys():
        if key not in IGNORE_KEYS and not key.startswith('entry_'):
            keys.append(key)
    db_answer.update( { 'survey_id': data['_survey_id'] }, 
                      { '$addToSet': { 'metas': { '$each' : keys} } } , upsert=True);            

def get_answer_count(survey_id):
    db_answer = mongo_wrapper.get_instance().usercafe.answers
    return db_answer.find({'_survey_id':survey_id,'$or':[{'_finished':{'$exists':False}},{'_finished':'true'}] }).count()

def insert_answer_data(data):
    import datetime
    data['_timestamp'] = datetime.datetime.now().isoformat()
    db_answer = mongo_wrapper.get_instance().usercafe.answers
    try:
        if '_survey_id' in data:
            insert_meta(data)
        db_answer.insert(data)
        return True
    except Exception,e:
        logging.error("Add app failed {0}".format(str(e)))
    return False
    
def delete_answer(survey_id):
    db_answer = mongo_wrapper.get_instance().usercafe.answers
    db_answer.remove({'survey_id':survey_id})
    
def generate_csv(titles,answers,output):
    entries = []
    title_str = '"'
    with open(output,"w") as wfp:
        for title in titles:
            title_str = title_str + title['displayName'] + '","'
            entries.append(title['field'])
        wfp.writelines(title_str[:-2].replace("\n","|").encode('utf8')+"\n")
        for answer in answers:
            answer_str = '"'
            for entry in entries:
                if entry in answer:
                    answer_str = answer_str + answer[entry] + '","'
                else:
                    answer_str = answer_str + '","'            
            wfp.writelines(answer_str[:-2].replace("\n","|").encode('utf8')+"\n")
    return True

def get_fileid(survey_id):
    db_google = mongo_wrapper.get_instance().usercafe.google_drive
    data = db_google.find_one({'survey_id':survey_id})
    if data and 'file_id' in data:
        return data['file_id']
    return None

def get_link_if_unmodified(survey_id):
    db_google = mongo_wrapper.get_instance().usercafe.google_drive
    data = db_google.find_one({'survey_id':survey_id})
    if data and 'last_count' in data and get_answer_count(survey_id) == data['last_count']:
        return data['link']
    return None

def modify_answer_record(survey_id,change_count=False,file_id=None,link=None):
    db_google = mongo_wrapper.get_instance().usercafe.google_drive
    data = {'survey_id':survey_id}
    if change_count:
        data['last_count'] = get_answer_count(survey_id)
    if file_id is not None:
        data['file_id'] = file_id
    if link is not None:
        data['link'] = link
    db_google.update({'survey_id':survey_id},{'$set':data},upsert=True)

def answer_escape(surveyid):
    arr = []
    db_q = mongo_wrapper.get_instance().usercafe.surveys
    qs = db_q.find_one({'_id':ObjectId(surveyid)})
    for v in qs['pages'].values():
        for q in v['questions']:
            if 'name' in q and q['name'].startswith('entry.'):
                arr.append({'title':q['title'],'name':q['name'].replace('.','_'),'cnt':0})
    if 'properties' in qs and 'demography' in qs['properties'] and int(qs['properties']['demography']) == 1:
        arr.append({'title':'gender','name':'_gender','cnt':0})
        arr.append({'title':'age','name':'_age','cnt':0})
        arr.append({'title':'occupation','name':'_occupation','cnt':0})
        
    db_answers = mongo_wrapper.get_instance().usercafe.answers
    answer_stat = {}
    answers = db_answers.find({'_survey_id':surveyid})
    cnt = 0
    idx = 0
    for answer in answers:
        for q in arr:
            if q['name'] in answer:
                q['cnt'] += 1
        cnt += 1
    return arr,cnt