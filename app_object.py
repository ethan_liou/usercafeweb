import pymongo
import logging
from bson.objectid import ObjectId
import survey_object
import mongo_wrapper

app_cache = {}

def check_write_permission(user,app_id):
    app = find_app_by_id(app_id)
    return app is not None and app.check_write_permission(user.email())      

def clear_cache(app_id):
    try:
        del app_cache[app_id]
    except:
        pass

def find_app_by_id(app_id):
    if app_id not in app_cache:
        db_apps = mongo_wrapper.get_instance().usercafe.apps
        data = db_apps.find_one({'_id':ObjectId(app_id)})
        if data is not None:
            app_cache[app_id] = App(data)
            return App(data)
    else:
        return app_cache[app_id]
    return None

def list_all_apps(user_email):
    email = user_email.replace('.','|')
    apps = {}        
    db_apps = mongo_wrapper.get_instance().usercafe.apps
    cur = db_apps.find({'permission.'+email:{'$exists':True}})
    for app in cur:
        keys = app['permission'].keys()
        for acc in keys:
            app['permission'][acc.replace('|','.')] = app['permission'][acc]
            del app['permission'][acc]
        app['_id'] = str(app['_id'])
        app['self_permission'] = app['permission'][user_email]
        apps[app['_id']] = App(app) 
    return apps
    
def insert_app(data):
    if 'user_id' in data:
        db_app = mongo_wrapper.get_instance().usercafe.apps
        try:
            app_id = db_app.insert(data)
            data['_id'] = str(app_id)
            return data
        except Exception as e:
            logging.error("Add app failed",str(e))
    return None

class App(object):
    def __init__(self,app_data):
        self.app_id = str(app_data['_id'])
        self.app_data = app_data
        # (id,survey) dict        
        self._surveys = None
    
    def surveys(self):
        if self._surveys is None:
            self._surveys = self._load_all_surveys()
        return self._surveys
    
    def get_answer_escape(self,survey_id):
        import answer_object
        return answer_object.answer_escape(survey_id)
    
    def check_write_permission(self,user_email):
        ret = False
        email = user_email.replace(".","|")
        if email in self.app_data['permission']:
            ret = self.app_data['permission'][email] in ['owner','admin']
        return ret
    
    def delete(self):
        if self.app_id:
            db_apps = mongo_wrapper.get_instance().usercafe.apps
            db_apps.remove({'_id':ObjectId(self.app_id)})        
    
    def is_mapping_conflict(self,user_defined_id):
        if self.app_id:
            db_apps = mongo_wrapper.get_instance().usercafe.apps
            app = db_apps.find_one({'_id':ObjectId(self.app_id)})
            if 'mapping' in app and user_defined_id in app['mapping'] or user_defined_id in self.surveys().keys():
                return True
            return False
            
    def get_survey_id_by_user_defined_id(self,user_defined_id):
        if self.app_id:
            db_apps = mongo_wrapper.get_instance().usercafe.apps
            app = db_apps.find_one({'_id':ObjectId(self.app_id)})
            if 'mapping' in app and user_defined_id in app['mapping']:
                return app['mapping'][user_defined_id]
        return None
    
    def update_mapping(self,survey_id,user_defined_id):
        if self.app_id:
            db_apps = mongo_wrapper.get_instance().usercafe.apps
            d = {}
            key_to_remove = []
            if 'mapping' in self.app_data:
                # user_defined id -> survey id
                for u_id,s_id in self.app_data['mapping'].iteritems():
                    if s_id == survey_id:
                        # remove old user_defined_id
                        d['mapping.'+u_id] = ""
                        key_to_remove.append(u_id)
            update = {'$set':{'mapping.'+user_defined_id:survey_id}}
            if len(d) > 0:
                update["$unset"]=d
            ret = db_apps.update({'_id':ObjectId(self.app_id)},update,upsert=True)    
            if ret['ok']:
                # create mapping if not existed
                if 'mapping' not in self.app_data:
                    self.app_data['mapping'] = {}
                    
                # remove old key
                for key in key_to_remove:
                    del self.app_data['mapping'][key]
                    
                # add new key
                self.app_data['mapping'][user_defined_id] = survey_id
                return True
        return False

    def update_permission(self,account,role):
        db_apps = mongo_wrapper.get_instance().usercafe.apps
        ret = db_apps.update({'_id':ObjectId(self.app_id)},{'$set':{'permission.'+account.replace(".","|"):role}},upsert=True)    
        if ret['ok']:
            self.app_data['permission'][account] = role
            return True
        return False

    def delete_permission(self,account):
        db_apps = mongo_wrapper.get_instance().usercafe.apps
        ret = db_apps.update({'_id':ObjectId(self.app_id)},{'$unset':{'permission.'+account.replace(".","|"):""}})
        print ret    
        if ret['ok']:
            del self.app_data['permission'][account]
            return True
        return False
    
    def update(self,data):
        if self.app_id:
            db_apps = mongo_wrapper.get_instance().usercafe.apps
            if '_id' in data.keys():
                del data['_id']
            ret = db_apps.update({'_id':ObjectId(self.app_id)},{'$set':data},upsert=True)
            if ret['ok']:
                self.app_data.update(data)
                return True
        return False
            
    def _load_all_surveys(self):
        return survey_object.list_all_surveys_simple(self.app_id)
    
    def list_all_surveys(self):
        surveys = []
        for survey in self.surveys().values():
            surveys.append(survey.survey_data)
        return surveys
    
    def get_survey_raw_data(self,survey_id_or_user_defined_id):
        if survey_id_or_user_defined_id in self.surveys().keys():
            return self.surveys()[survey_id_or_user_defined_id].get_raw_data()
        survey_id = self.get_survey_id_by_user_defined_id(survey_id_or_user_defined_id)
        if survey_id is not None and survey_id in self.surveys().keys():
            return self.surveys()[survey_id].get_raw_data()
        return None
    
    
    '''
    def get_survey_detail(self,survey_id):
        if survey_id in self.surveys().keys():
            return self.surveys()[survey_id].get_detail()
        return None
    '''
    
    def delete_survey(self,survey_id):
        if survey_id in self.surveys().keys():
            self.surveys()[survey_id].delete()
            clear_cache(self.app_id)
        # [TODO] error handling...

    def add_google_survey(self,url):
        return self.add_survey(survey_object.convert_survey_from_google(url))

    def update_properties_info(self,survey_id,properties):
        if survey_id in self.surveys().keys():
            for key in properties.keys():
                properties['properties.'+key] = properties[key]
                del properties[key]
            return self.surveys()[survey_id].update(properties) 
        return False

    def add_survey(self,data):
        if data is not None:
            import time
            data['update_time'] = int(time.time())        
            data['app_id'] = self.app_id
            survey = survey_object.insert_survey(data)
            if survey:
                self.surveys()[survey.survey_data['_id']] = survey
                clear_cache(self.app_id)
                return survey            
        return None
        
    def update_survey_info(self,survey_id,data):
        if survey_id in self.surveys().keys():
            self.surveys()[survey_id].update(data)
        # [TODO] error handling...    
