import json
from flask import session
from urllib2 import Request, urlopen, URLError
import user_object
from functools import wraps

SESSION_KEY_EMAIL = 's_email'
SESSION_KEY_ID = 's_id'

ADMIN = ['lyccaliu@gmail.com','usercafe.share@gmail.com','partylogo@gmail.com','petersu@gogolook.com.tw','dreamydiva999@gmail.com','swchen11@gmail.com']

def get_user_id(session):
    user_id = None
    if SESSION_KEY_ID in session:
        user_id = session[SESSION_KEY_ID]
    return user_id

def is_ad_valid_session(session):
    return SESSION_KEY_EMAIL in session and session[SESSION_KEY_EMAIL] in ADMIN

def is_ad_valid_user(user):
    return user.user_data['email'] in ADMIN

def get_user(session):
    user = None
    if SESSION_KEY_EMAIL in session:
        user = user_object.get_user_by_email(session[SESSION_KEY_EMAIL])
    return user

def login(token,session):
    email = get_email_by_google_token(token)
    if email is not None:
        session[SESSION_KEY_EMAIL] = email
        user = get_user(session)
        if user is None:
            # new user...
            user = user_object.add_user({'email':email})
        session[SESSION_KEY_ID] = user.user_id
        session.modified = True
        session.permanent = True
        
        return user.user_data
    return None

def logout(session):
    session.pop(SESSION_KEY_EMAIL)
    session.pop(SESSION_KEY_ID)
    session.modified = True
    
def _get_email_from_response(res):
    try:
        js = json.loads(res)
        return js['email']
    except Exception, e:
        print e
    return None

def get_email_by_google_token(token):

    headers = {'Authorization': 'OAuth '+token}
    req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                  None, headers)
    account = None
    try:
        res = urlopen(req)
    except URLError, e:
        if e.code == 401:
            return None
        account = _get_email_from_response(res.read())
    account = _get_email_from_response(res.read())   
    return account