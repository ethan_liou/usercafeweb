from flask import Blueprint, request, session,Response,make_response,jsonify
from utils import *
import user_object
import json
import app_object
import utils

app_bp = Blueprint('app_bp',__name__)

# app parts
@app_bp.route('',methods=['GET'])
@user_granted()
def list_all_apps(user):
    apps = user.list_all_apps()
    return Response(json.dumps(apps),  mimetype='application/json')

@app_bp.route('',methods=['POST'])
@user_granted()
@check_params('name')
def add_apps(user,post_data,name):
    data_dict = user.add_app(post_data)
    if data_dict is not None:
        return jsonify(**data_dict)
    return make_response("",400)    

@app_bp.route('/<appid>',methods=['DELETE'])
@user_granted(True)
def delete_app(user,appid):
    return user.delete_app(appid) and make_response("OK",200) or make_response("Invalid appid",400)

@app_bp.route('/<appid>',methods=['POST'])
@user_granted(True)
@check_params()
def update_app(appid,user,post_data):                 
    return user.update_app_info(appid,post_data) and make_response("",200) or make_response("Invalid appid",400)         
    
@app_bp.route('/<appid>/modify_mapping',methods=['POST'])
@user_granted(True)
@check_params('survey_id','user_defined_id')
def modify_app_mapping(appid,user,post_data,survey_id,user_defined_id):
    return user.update_survey_mapping(appid,survey_id,user_defined_id) and make_response("",200) or make_response("Invalid params",400)

@app_bp.route('/<appid>/permission',methods=['POST'])
@user_granted(True)
@check_params('account','role')
def modify_app_permission(appid,user,post_data,account,role):
    return user.update_permission(appid,account,role) and make_response("",200) or make_response("Invalid params",400) 

@app_bp.route('/<appid>/delete_permission',methods=['POST'])
@user_granted(True)
@check_params('account')
def delete_app_permission(appid,user,post_data,account):
    return user.delete_permission(appid,account) and make_response("",200) or make_response("Inavlid params", 400)
