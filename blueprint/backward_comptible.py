

# backward compatible
OLD_APP_ID = '520100d02a4f560ef3e3fe0b'

user_object.load_user_into_cache()

def get_survey_id_from_form_key(form_key):
    form_key = str(form_key)
    mc = memcache.Client([MEMCACHE_SERVER],debug=0)
    survey_id = mc.get(form_key)
    if survey_id is None:
        # get from db
        survey_id = survey_object.get_survey_id_from_form_key(form_key)
        if survey_id is not None:
            mc.set(form_key,survey_id)
    return survey_id
    
@application.route('/get_content')
def old_get_json():
    form_key = request.args.get('form_key',None)
    if form_key is not None:
        data = survey_object.get_survey_from_form_key(form_key)
        if data is not None:
            return jsonify(**data)
    return make_response("",400)

@application.route('/upload_answer',methods=['POST'])
def old_upload_answer():
    data = request.form.to_dict()
    if data is not None and '_form_key' in data.keys():
        survey_id = get_survey_id_from_form_key(data['_form_key'])
        if survey_id is not None and survey_object.is_valid_survey(OLD_APP_ID,survey_id):
            del data['_form_key']
            data['_survey_id'] = survey_id
            answer_object.insert_answer_data(data)
            if '_device_id' in data.keys():
                mc = memcache.Client([MEMCACHE_SERVER],debug=0)
                mc.delete('user_'+str(data['_device_id']))   
            answer_object.modify_answer_record(survey_id,False)
            return make_response("",200)
    return make_response("",400)    