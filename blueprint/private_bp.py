from flask import Blueprint, request, session,Response,make_response,jsonify
import user_object
import app_object
import json

private_bp = Blueprint('private_bp',__name__)

# special for testing...
@private_bp.route('/user/<user_mail>/app',methods=['GET'])
def list_private_all_apps(user_mail):
    u = user_object.get_user_by_email(user_mail,True)
    if u is not None:
        apps = u.list_all_apps()
        return Response(json.dumps(apps),  mimetype='application/json')
    return make_response("",400)

@private_bp.route('/app/<appid>/survey',methods=['GET'])
def list_private_surveys(appid):
    a = app_object.find_app_by_id(appid)
    if a is not None:
        surveys = a.list_all_surveys()
        return Response(json.dumps(surveys),  mimetype='application/json') 
    return make_response("",400)
