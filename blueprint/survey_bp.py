from flask import Blueprint, request, session,Response,make_response,jsonify
from utils import *
import user_object
import json
import app_object
import utils

survey_bp = Blueprint('survey_bp',__name__)

# appid will pass in
def handle_app(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user = kwargs[utils.USER_KEY]
        if 'appid' in kwargs and user.app(kwargs['appid']):
            kwargs['app'] = user.app(kwargs['appid'])
            del kwargs['appid']
            del kwargs[utils.USER_KEY]
            return f(*args,**kwargs)
        return make_response("Invalid appid",400)
    return decorated_function

@survey_bp.route('',methods=['GET'])
@user_granted()
@handle_app
def list_surveys(app):
    surveys = app.list_all_surveys()
    return Response(json.dumps(surveys),  mimetype='application/json') 

@survey_bp.route('/googledoc',methods=['POST'])
@user_granted()
@handle_app
@check_params('url')
def convert_googledoc(app,post_data,url):
    survey = app.add_google_survey(url)
    if survey:
        return jsonify(**survey.survey_data)
    return make_response("Unknown error",500)

@survey_bp.route('/<surveyid>/properties',methods=['POST'])
@user_granted(True)
@handle_app
@check_params()
def set_properties(app,surveyid,post_data):
    return app.update_properties_info(surveyid,post_data) and make_response("",200) or make_response("Invalid param",400)