from flask import make_response,request
import auth_helper
from functools import wraps
import user_object
import app_object

USER_KEY = 'user'
POST_DATA_KEY = 'post_data'

def user_granted(check_app_permission=False):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            #user_id = auth_helper.get_user_id(session)
            user_id = '5200ff2351faa63812004f64'

            err = 'You must login'            
            if user_id:
                err = None
                user = user_object.get_user_by_id(user_id)
                kwargs[USER_KEY] = user
                
                # check app
                if check_app_permission and not app_object.check_write_permission(user,kwargs['appid']):
                    err = 'You have no permission'
            return err is None and f(*args, **kwargs) or make_response(err,403)
        return decorated_function
    return decorator

def check_params(*necessary_params):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            data = request.json
            err = 'Data is necessary'
            if data:
                err = None
                for key in necessary_params:
                    if not data.has_key(key):
                        err = '{0} is necessary'.format(key)
                        break
                    kwargs[key] = data[key]
                kwargs[POST_DATA_KEY] = data
            return err is None and f(*args, **kwargs) or make_response(err,400)
        return decorated_function
    return decorator