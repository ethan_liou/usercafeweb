

DEMOGRAPHY_CHINESE_JSON="""
[
    {
      "title": "\u57fa\u672c\u8cc7\u6599",
      "required": 0,
      "type": 7,
      "name": "PageBreaker",
      "desc": ""
    },
    {
      "name": "_gender",
      "title": "1. \u8acb\u554f\u60a8\u7684\u6027\u5225\u70ba",
      "required": 1,
      "type": 2,
      "options": [
        "\u7537\u6027",
        "\u5973\u6027"
      ],
      "desc": ""
    },
    {
      "name": "_age",
      "title": "2. \u8acb\u554f\u60a8\u7684\u5e74\u9f61\u70ba",
      "required": 1,
      "type": 4,
      "options": [
        "15\u6b72\u4ee5\u4e0b",
        "16-20\u6b72",
        "21-25\u6b72",
        "26-30\u6b72",
        "31-35\u6b72",
        "36-40\u6b72",
        "41-45\u6b72",
        "46-50\u6b72",
        "51-55\u6b72",
        "56-60\u6b72",
        "61-65\u6b72",
        "66-70\u6b72",
        "71\u6b72\u4ee5\u4e0a"
      ],
      "desc": ""
    },
    {
      "name": "_occupation",
      "title": "3. \u8acb\u554f\u60a8\u7684\u8077\u696d\u70ba",
      "required": 1,
      "type": 4,
      "options": [
        "\u5b78\u751f",
        "\u5bb6\u7ba1",
        "\u9000\u4f11",
        "\u5f85\u696d",
        "\u8fb2\u3001\u6797\u3001\u6f01\u3001\u7267\u696d",
        "\u7926\u696d\u53ca\u571f\u77f3\u63a1\u53d6\u696d",
        "\u88fd\u9020\u696d",
        "\u96fb\u529b\u53ca\u71c3\u6c23\u4f9b\u61c9\u696d",
        "\u7528\u6c34\u4f9b\u61c9\u53ca\u6c61\u67d3\u6574\u6cbb\u696d",
        "\u71df\u9020\u696d",
        "\u6279\u767c\u53ca\u96f6\u552e\u696d",
        "\u904b\u8f38\u8207\u5009\u5132\u696d",
        "\u4f4f\u5bbf\u8207\u9910\u98f2\u696d",
        "\u8cc7\u8a0a\u8207\u901a\u8a0a\u50b3\u64ad\u696d",
        "\u91d1\u878d\u8207\u4fdd\u96aa\u696d",
        "\u4e0d\u52d5\u7522\u696d",
        "\u5c08\u696d\u3001\u79d1\u5b78\u53ca\u6280\u8853\u670d\u52d9\u696d",
        "\u652f\u63f4\u670d\u52d9\u696d",
        "\u516c\u5171\u884c\u653f\u53ca\u570b\u9632\uff1b\u5f37\u5236\u6027\u793e\u6703\u5b89\u5168",
        "\u6559\u80b2\u670d\u52d9\u696d",
        "\u91ab\u7642\u4fdd\u5065\u53ca\u793e\u6703\u5de5\u4f5c\u670d\u52d9\u696d",
        "\u85dd\u8853\u3001\u5a1b\u6a02\u53ca\u4f11\u9592\u670d\u52d9\u696d",
        "\u5176\u4ed6\u670d\u52d9\u696d",
        "\u5176\u4ed6\u4e0d\u80fd\u6b78\u985e\u4e4b\u884c\u696d"
      ],
      "desc": ""
    }
]
"""