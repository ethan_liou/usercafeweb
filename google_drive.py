#!/usr/bin/python

import httplib2
import pprint

from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import run

# Copy your credentials from the APIs Console
CLIENT_ID = '532087134822.apps.googleusercontent.com'
CLIENT_SECRET = 'DSbGpxIlvpQggpB_zN2ZVNgw'

# Check https://developers.google.com/drive/scopes for all available scopes
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'
try:
    import uwsgi
    OAUTH2_STORAGE = '/opt/oauth/oauth2.dat'
except:
    OAUTH2_STORAGE = '/opt/oauth/oauth2_no_uwsgi.dat'

# Redirect URI for installed apps
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

def get_drive():
    # Run through the OAuth flow and retrieve credentials
    flow = OAuth2WebServerFlow(CLIENT_ID, CLIENT_SECRET, OAUTH_SCOPE, REDIRECT_URI)
    storage = Storage(OAUTH2_STORAGE)
    credentials = storage.get()

    if credentials is None or credentials.invalid:
        authorize_url = flow.step1_get_authorize_url()
        print 'Go to the following link in your browser: ' + authorize_url
        code = raw_input('Enter verification code: ').strip()
        credentials = flow.step2_exchange(code)
        storage.put(credentials)
    # Create an httplib2.Http object and authorize it with our credentials
    http = httplib2.Http()
    http = credentials.authorize(http)

    drive_service = build('drive', 'v2', http=http)
    return drive_service

gdrive_device = get_drive()

def get_file_url(file_id):
    fp = gdrive_device.files().get(fileId=file_id).execute()
    return fp.get('alternateLink','')

def retrieve_permissions( file_id):
    try:
        permissions = gdrive_device.permissions().list(fileId=file_id).execute()
        return permissions.get('items', [])
    except errors.HttpError, error:
        print 'An error occurred: %s' % error
    return None

# role : {account : permission}
def add_permission_to_id(role_dict,file_id):
    permission_map = {
        'viewer':'reader',
        'admin':'writer',
        'owner':'writer'
    }
    for acc,role in role_dict.iteritems():
        print acc,role
        new_permission = {
            'value': acc,
            'type': 'user',
            'role': permission_map[role]
        }
        gdrive_device.permissions().insert(fileId=file_id, body=new_permission).execute()


def create_a_file_with_title(title,account):
    RETRY_TIME = 5
    for i in range(RETRY_TIME):
        try:
            body = {
                'mimeType': 'application/vnd.google-apps.spreadsheet',
                'title': title,
            }
            sheet = gdrive_device.files().insert(body=body).execute()
            add_permission_to_id({account:"owner"},sheet['id'])
            break
        except Exception,e:
            print 'Create file error, {0} times, error {1}'.format(i,str(e))
    if sheet is None:
        raise Exception('create file error...')
    return sheet['id'],sheet['alternateLink']

def delete_file(file_id):
    gdrive_device.files().delete(fileId=file_id).execute()

def list_files():
    files = gdrive_device.files().list().execute()
    for item in files['items']:
        print item
        #print item['title'],item['id']

def upload_file(title,file_path,account,role,file_id = None):
    RETRY_TIME = 5
    for i in range(RETRY_TIME):
        try:
            data = MediaFileUpload(file_path, mimetype='text/csv')
            body = {
                'mimeType': 'text/csv',
                'title': title,
            }
            if file_id is None:
                sheet = gdrive_device.files().insert(body=body,media_body=data,convert=True).execute()
            else:
                sheet = gdrive_device.files().update(fileId=file_id,body=body,media_body=data,convert=True).execute()                
            add_permission_to_id(role,sheet['id'])                
            break
        except Exception,e:
            print 'Create file error, {0} times, error {1}'.format(i,str(e))
    if sheet is None:
        raise Exception('create file error...')
    return sheet['id'],sheet['alternateLink']

