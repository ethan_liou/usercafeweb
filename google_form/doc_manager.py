from page_manager import *
from pyquery import PyQuery as pq
from utils import *

class DocManager(object):
    def __init__(self,url):
        self._url = url
        self._pages = []
        self._page_nums = []
        self._title = None
        self._desc = None
        self._version = -1
        
    def _get_page_manager(self,**kwargs):
        print self._version
        if self._version == 1:
            return PageManagerV1(**kwargs)
        elif self._version == 2:
            return PageManagerV2(**kwargs)
        else:
            raise RuntimeError("Not support version")
        
    def _get_version(self):
        content = get_content_follow_redirect(self._url)   
        d = pq(content)
        hidden_attrs = d('input[type=hidden]')
        if len(hidden_attrs) >= 2:
            hidden_set = set([hidden_attrs[0].get('name'),hidden_attrs[1].get('name')])
            if 'draftResponse' in hidden_set and 'pageHistory' in hidden_set:
                self._version = 2
            elif 'backupCache' in hidden_set and 'pageNumber' in hidden_set:
                self._version = 1
        
    def _load_first_page(self):
        first_page = self._get_page_manager(page=0,url=self._url)
        self._title,self._desc = first_page.parse_content(True)

        self._pages.append(first_page)
        self._page_nums.append(0)
        return first_page

    def _get_form_key(self,url):
        import urlparse
        d = urlparse.urlparse(url)
        if self._version == 1:
            query_str = d.query
            form_key = urlparse.parse_qs(query_str)['formkey'][0]
            return form_key.split('&')[0] # remove &ifq
        elif self._version == 2:
            return d.path.split('/')[3] # TODO not sure...


    def parse_doc(self):
        self._get_version()
        print "get version done"
        first_page = self._load_first_page()
        print "load first page done"
        if first_page is None:
            return None
        idx = 0
        while(idx < len(self._pages)):
            print 'current page num',self._page_nums[idx]
            current_page = self._pages[idx]
            if current_page._mode == PageManager.MODE_MID_PAGE:
                page_dict = current_page.test_next_page()
                if page_dict is None:
                    print 'error'
                    return None
                for page_num in page_dict.keys():
                    if not page_num in self._page_nums:
                        pm = self._get_page_manager(content=page_dict[page_num])
                        pm.parse_content(False)
                        self._pages.append(pm)
                        self._page_nums.append(page_num)
            else:
                print 'submit page'
            idx += 1
                
        try:
            pages = {}
            
            max_page_num = max(self._page_nums)
            min_page_num = min(self._page_nums)
            for page_num in self._page_nums:
                # 0 ~ total_page - 1
                idx = self._page_nums.index(page_num)
                if min_page_num < 0:
                    self._pages[idx].replace_pages_to_submit(min_page_num,max_page_num+1)
                if min_page_num < 0 and min_page_num == page_num:
                    pages[str(max_page_num+1)] = self._pages[idx].to_dict()
                else:
                    pages[str(page_num)] = self._pages[idx].to_dict()
        except Exception,e :
            print str(e)
        self._page_nums.sort()
        
        # backward compatible start 
        max_page_num = -1
        if self._page_nums[-1] == 99999:
            max_page_num = int(self._page_nums[-2])+1
            pages[str(max_page_num)] = pages['99999']
            del pages['99999']
            self._page_nums[-1] = max_page_num
        # backward compatible end            
        
        for num in self._page_nums:
            # backward compatible start
            if pages[str(num)]['default_next_page'] == 99999:
                pages[str(num)]['default_next_page'] = max_page_num
            for jmp_page in pages[str(num)]['jump_page']:
                if jmp_page == 99999:
                    jmp_page_idx = pages[str(num)]['jump_page'].index(jmp_page)
                    pages[str(num)]['jump_page'][jmp_page_idx] = max_page_num
            # backward compatible end
            
            idx = self._page_nums.index(num)
            if pages[str(num)]['default_next_page'] == -999:
                if idx != len(self._page_nums) - 1:
                    # not last page
                    pages[str(num)]['default_next_page'] = int(self._page_nums[idx+1])
                else:
                    pages[str(num)]['default_next_page'] = -1
        d = {'version':self._version,
             'title':self._title,
             'desc':self._desc,
             'action_url':first_page._action_url,
             'form_key':self._get_form_key(first_page._action_url),
             'pages':pages,
             'url':self._url}
        return d

def prepare_headers(js):
    headers = ['Timestamp']
    page_cnt = len(js['pages'].keys())
    for page_no in range(page_cnt):
        questions = js['pages'][str(page_no)]['questions']
        question_cnt = len(questions)
        for q_no in range(question_cnt):
            question = questions[q_no]
            if question['type'] == 6 and 'title' in question:
                # grid
                for row in question['row_labels']:
                    headers.append('{0}[{1}]'.format(question['title'],row))
            elif question['type'] <= 6 and 'title' in question:
                headers.append(question['title'])
    return headers

#FORM_URL='https://docs.google.com/forms/d/1iSj5FnrLC4nqDWbPAbh4j4N7XdHeA8zoKTa6Dwiikfw/viewform?edit_requested=true'
#manager = DocManager(FORM_URL)
#doc = manager.parse_doc()
