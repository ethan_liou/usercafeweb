import urllib

import utils

TYPE_TEXT, \
TYPE_LONG_TEXT, \
TYPE_MULTIPLECHOICE, \
TYPE_CHECKBOX, \
TYPE_LIST, \
TYPE_SCALE, \
TYPE_GRID, \
TYPE_PAGE_BREAKER, \
TYPE_SECTION_HEADER, \
TYPE_SUBMIT, \
TYPE_IMAGE, \
TYPE_DATE, \
TYPE_TIME = range(13)

class EntryTypeFactory(object):
    @classmethod
    def newInstance(cls,js):
        if js['type'] == TYPE_TEXT:
            return TextEntry(None,js)
        if js['type'] == TYPE_LONG_TEXT:
            return LongTextEntry(None,js)
        if js['type'] == TYPE_MULTIPLECHOICE:
            return MultipleChoiceEntry(None,js)
        if js['type'] == TYPE_CHECKBOX:
            return CheckBoxEntry(None,js)
        if js['type'] == TYPE_LIST:
            return ListEntry(None,js)
        if js['type'] == TYPE_SCALE:
            return ScaleEntry(None,js)
        if js['type'] == TYPE_GRID:
            return GridEntry(None,js)
        if js['type'] == TYPE_PAGE_BREAKER:
            return PageBreakEntry(None,js)
        if js['type'] == TYPE_SECTION_HEADER:
            return SectionHeaderEntry(None,js)
        if js['type'] == TYPE_SUBMIT:
            return SubmitEntry(None,js)
        if js['type'] == TYPE_IMAGE:
            return ImageContainerEntry(None,js)
        if js['type'] == TYPE_DATE:
            return DateEntry(None,js)
        if js['type'] == TYPE_TIME:
            return TimeEntry(None,js)

class EntryFactory(object):
    @classmethod
    def newInstance(cls,entry):
        cls_attr = entry.get('class')
        if cls_attr is None:
            return None
        if 'ss-text' in cls_attr:
            return TextEntry(entry)
        elif 'ss-date' in cls_attr:
            return DateEntry(entry)
        elif 'ss-time' in cls_attr:
            return TimeEntry(entry)
        elif 'ss-image-container' in cls_attr or 'ss-embeddable-object-container' in cls_attr:
            return ImageContainerEntry(entry)
        elif 'ss-paragraph-text' in cls_attr:
            return LongTextEntry(entry)
        elif 'ss-radio' in cls_attr:
            return MultipleChoiceEntry(entry)
        elif 'ss-checkbox' in cls_attr:
            return CheckBoxEntry(entry)
        elif 'ss-select' in cls_attr:
            return ListEntry(entry)
        elif 'ss-scale' in cls_attr:
            return ScaleEntry(entry)
        elif 'ss-grid' in cls_attr:
            return GridEntry(entry)
        elif 'ss-navigate' in cls_attr:
            return SubmitEntry(entry)
        elif 'ss-section-header' in cls_attr:
            return SectionHeaderEntry(entry)
        elif 'ss-page-break' in cls_attr:
            return PageBreakEntry(entry)
        return BaseEntry(entry)

def checkClass(cls_name,entry_class_attr):
    if entry_class_attr is not None and cls_name in entry_class_attr:
        return True
    else:
        return False

ENTRY_TITLE_CLASS_NAME = 'ss-q-title'
ENTRY_DESC_CLASS_NAME = 'ss-q-help'
class BaseEntry(object):
    version = -1
    def __init__(self,entry,js=None):
        if js is None:
            self._title = None
            self._desc = None
            self._required = False
            self._type = None
            self._name = None
            
            self._get_title(entry)
            self._get_desc(entry)
            self._get_required(entry)
        else:
            self._title = utils.get_data_from_dict(js,'title',None)
            self._desc = utils.get_data_from_dict(js,'desc',None)
            self._required = utils.get_data_from_dict(js,'required',None)
            self._name = utils.get_data_from_dict(js,'name',None)
            self._type = utils.get_data_from_dict(js,'type',None)
            self.jump_arr = None

    def _get_required(self,entry):
        for span in entry.getiterator():
            if checkClass('ss-required-asterisk',span.get('class')):
                self._required = True

    def _get_title(self,entry):
        for e in entry.getiterator():
            if checkClass(ENTRY_TITLE_CLASS_NAME,e.get('class')):
                self._title = e.text_content().rstrip().encode('utf8')
                self._title = self._title.split('\n')[0]
                return
    
    def _get_desc(self,entry):
        for e in entry.getiterator():
            if checkClass(ENTRY_DESC_CLASS_NAME,e.get('class')):
                self._desc = e.text_content().rstrip().encode('utf8')
                self._desc = self._desc.split('\n')[0]
                return
    
    def __str__(self):
        if self._title is None:
            return 'None'
        s = self._title + ',' + self._desc
        if self._required:
            s = '*,'+s
        if not self._name is None:
            s = self._name+',' + s
        return s

    def random_answer(self,form_dict):
        form_dict[self._name] = '0'

    def _add_to_dict_when_is_not_null(self,d,k,v):
        if v is not None:
            d[k] = v

    def to_dict(self):
        if self._required:
            require_int = 1
        else:
            require_int = 0
        d = {}
        self._add_to_dict_when_is_not_null(d,'title',self._title)
        self._add_to_dict_when_is_not_null(d,'desc',self._desc)
        self._add_to_dict_when_is_not_null(d,'required',require_int)
        self._add_to_dict_when_is_not_null(d,'name',self._name)
        self._add_to_dict_when_is_not_null(d,'type',self._type)
        return d

    def to_html(self):
        # title
        html = u'<h1>{0}</h1>'.format(self._title)
        # desc
        html = html + u'<p>{0}</p>'.format(self._desc)
        return html

class ImageContainerEntry(BaseEntry):
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._type = TYPE_IMAGE
            self._img_url = None
            for img in entry.getiterator('img'):
                import base64,urllib
                image = urllib.urlopen(img.get('src'))
                self._img_data = base64.encodestring(image.read())
        else:
            self._img_data = utils.get_data_from_dict(js,'img_data',None)
                        
    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['img_data'] = self._img_data
        return d
            
class PageBreakEntry(BaseEntry):
    def __init__(self,entry,js=None):
        if js is None:
            self._title = None
            self._desc = None
            self._required = False
            # title
            for h2 in entry.getiterator('h2'):
                if h2 is None:
                    continue
                if checkClass('ss-page-title',h2.get('class')):
                    self._title = h2.text_content().rstrip().encode('utf8')
            # desc
            for div in entry.getiterator('div'):
                if div is None:
                    continue
                if checkClass('ss-page-description',div.get('class')):
                    self._desc = div.text_content().rstrip().encode('utf8')   
                    
            if self._title is None:
                self._title = ''
            if self._desc is None:
                self._desc = ''
            self._type = TYPE_PAGE_BREAKER
            self._name = 'PageBreaker'
        else:
            BaseEntry.__init__(self,entry,js)

class SectionHeaderEntry(BaseEntry):
    def __init__(self,entry,js=None):
        if js is None:
            self._title = None
            self._desc = None
            self._required = False
            # name
            for h2 in entry.getiterator('h2'):
                if h2 is None:
                    continue
                if checkClass('ss-section-title',h2.get('class')):
                    self._title = h2.text_content().rstrip().encode('utf8')
            # desc
            for div in entry.getiterator('div'):
                if div is None:
                    continue
                if checkClass('ss-section-description',div.get('class')):
                    self._desc = div.text_content().rstrip().encode('utf8')   
                    
            if self._title is None:
                self._title = ''
            if self._desc is None:
                self._desc = ''
            self._type = TYPE_SECTION_HEADER
            self._name = 'SectionHeader'
        else:
            BaseEntry.__init__(self,entry,js)
        
TIME_TYPE_NORMAL = 0
TIME_TYPE_DURATION = 1
class TimeEntry(BaseEntry):
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._type = TYPE_TIME
            self._time_type = None
            self._load_time_type_and_name(entry)
        else:
            self._time_type = utils.get_data_from_dict(js,'time_type',None)
            print 'type',self._time_type
            
    def _load_time_type_and_name(self,entry):
        for div in entry.getiterator('div'):
            if 'ss-q-time' in div.get('class'):
                for select in div.getiterator('select'):
                    if len(select.value_options) > 20:
                        self._time_type = TIME_TYPE_DURATION
                    else:
                        self._time_type = TIME_TYPE_NORMAL
                    self._name = select.get('name').encode('utf-8').split('_')[0]
                    return
        #for i in entry.getiterator('input'):
        #    if 'time' in i.get('type'):
        #        self._time_type = TIME_TYPE_NORMAL
        #        self._name = i.get('name').encode('utf-8')
        #        return
            
    def random_answer(self,form_dict):
        if self._time_type == TIME_TYPE_NORMAL:
            form_dict[self._name] = '01:00'
        elif self._time_type == TIME_TYPE_DURATION:
            form_dict[self._name+"_hour"] = '1'
            form_dict[self._name+"_minute"] = '00'
            form_dict[self._name+"_second"] = '01'
            
    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['time_type'] = self._time_type
        return d        
           
DATE_TYPE_YEAR_MONTH_DAY = 0
DATE_TYPE_YEAR_MONTH_DAY_TIME = 1
DATE_TYPE_MONTH_DAY = 2
DATE_TYPE_MONTH_DAY_TIME = 3
class DateEntry(BaseEntry):
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._type = TYPE_DATE
            self._date_type = None
            self._load_time_type_and_name(entry)
            print 'type',self._date_type
        else:
            self._date_type = utils.get_data_from_dict(js,'date_type',None)
        
    def random_answer(self,form_dict):
        if self._date_type == DATE_TYPE_YEAR_MONTH_DAY:
            form_dict[self._name] = '2013-01-01'
        elif self._date_type == DATE_TYPE_MONTH_DAY:
            form_dict[self._name+"_month"] = '1'
            form_dict[self._name+"_day"] = '1'
        elif self._date_type == DATE_TYPE_YEAR_MONTH_DAY_TIME:
            form_dict[self._name] = '2013-01-01T00:00'
        elif self._date_type == DATE_TYPE_MONTH_DAY_TIME:
            form_dict[self._name+"_month"] = '1'
            form_dict[self._name+"_day"] = '1'
            form_dict[self._name+"_hour"] = '01'
            form_dict[self._name+"_minute"] = '00'
            form_dict[self._name+"_ampm"] = 'AM'
        
    def _load_time_type_and_name(self,entry):
        has_year = False
        for s in entry.getiterator('select'):
            if s.get('name').endswith('year'):
                has_year = True
        for div in entry.getiterator('div'):
            if 'ss-q-datetime' in div.get('class'):
                if has_year:
                    self._date_type = DATE_TYPE_YEAR_MONTH_DAY_TIME
                else:
                    self._date_type = DATE_TYPE_MONTH_DAY_TIME
                for select in div.getiterator('select'):
                    self._name = select.get('name').encode('utf-8').split('_')[0]
                    return                
            if 'ss-q-date' in div.get('class'):
                if has_year:
                    self._date_type = DATE_TYPE_YEAR_MONTH_DAY
                else:
                    self._date_type = DATE_TYPE_MONTH_DAY
                for select in div.getiterator('select'):
                    self._name = select.get('name').encode('utf-8').split('_')[0]
                    return
            
    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['date_type'] = self._date_type
        return d          
            
class TextEntry(BaseEntry):
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._type = TYPE_TEXT 
            # get name
            for i in entry.getiterator('input'):
                self._name = i.get('name').encode('utf8')
                break
    def to_html(self):
        html = BaseEntry.to_html(self)
        html = html + u'<input type="text" name="{0}" value=""  />'.format(self._name)
        return html

class LongTextEntry(BaseEntry):
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._type = TYPE_LONG_TEXT        
            # get name
            for i in entry.getiterator('textarea'):
                self._name = i.get('name').encode('utf8')
                break

    def to_html(self):
        html = BaseEntry.to_html(self)
        html = html + u'<textarea name="{0}"></textarea>'.format(self._name)
        return html

class MultipleChoiceEntry(BaseEntry):
    _options = None
    _next_page = None
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._options = []
            self._next_page = []
            self._type = TYPE_MULTIPLECHOICE
            # get name
            for i in entry.getiterator('input'):
                self._name = i.get('name').encode('utf8')
                break
    
            # get radio opt
            for radio in entry.getiterator('input'):
                value = radio.get('value').encode('utf8')
                if len(value) != 0:
                    self._options.append(value)
                    self._next_page.append(None)
        else:
            self._options = utils.get_data_from_dict(js,'options',None)

    def random_answer(self,form_dict):
        form_dict[self._name] = self._options[0]

    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['options'] = self._options
        return d        

    def to_html(self):
        html = BaseEntry.to_html(self)
        s = ''
        for i in range(len(self._options)):
            opt = self._options[i]
            if self._options[i] == '__option__' or self._options[i] == '__other_option__':
                opt = u'Other'
            checked = u''
            if i == 0:
                checked = u'checked="checked"'
            jump_page = ''
            if self.jump_arr is not None:
                jump_page = u"jump_page={0}".format(self.jump_arr[i])
            print 'jump',jump_page
            s = s + u'<input type="radio" name="{3}" id="radio-choice-{0}" {4} value="{1}" {2}/><label for="radio-choice-{0}">{1}</label>'\
            .format(i,opt,checked,self._name,jump_page)
        html = html + u'<fieldset data-role="controlgroup" name="{0}">{1}</fieldset>'.format(self._name,s)
        return html    

class CheckBoxEntry(BaseEntry):
    _options = None
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._options = []
            self._type = TYPE_CHECKBOX
            # get name
            for i in entry.getiterator('input'):
                self._name = i.get('name').encode('utf8')
                break
    
            # get checkbox opt
            for checkbox in entry.getiterator('input'):
                if checkbox.get('type') != 'checkbox':
                    continue
                self._options.append(checkbox.get('value').encode('utf8'))
        else:
            self._options = utils.get_data_from_dict(js,'options',None)

    def random_answer(self,form_dict):
        form_dict[self._name] = self._options[0]

    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['options'] = self._options
        return d        

    def to_html(self):
        html = BaseEntry.to_html(self)
        s = u''
        for i in range(len(self._options)):
            opt = self._options[i]
            if self._options[i] == '__option__' or self._options[i] == '__other_option__':
                opt = u'Other'            
            s = s + u'<input type="checkbox" name="{2}" id="checkbox-{0}" value="{1}"/><label for="checkbox-{0}">{1}</label>' \
            .format(i,opt,self._name)
        html = html + u'<fieldset data-role="controlgroup">{0}</fieldset>'.format(s)
        return html

class ListEntry(BaseEntry):
    _options = None
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._options = []
            self._type = TYPE_LIST
            # get name
            for i in entry.getiterator('select'):
                self._name = i.get('name').encode('utf8')
                break
    
            # get list opt
            for l in entry.getiterator('option'):
                txt = l.get('value').encode('utf8')
                if txt == '':
                    continue
                self._options.append(l.get('value').encode('utf8'))
        else:
            self._options = utils.get_data_from_dict(js,'options',None)

    def random_answer(self,form_dict):
        form_dict[self._name] = self._options[0]

    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['options'] = self._options
        return d        

    def to_html(self):
        html = BaseEntry.to_html(self)
        s = u''
        for i in range(len(self._options)):
            opt = self._options[i]
            if self._options[i] == '__option__' or self._options[i] == '__other_option__':
                opt = u'Other'
            checked = ''
            if i == 0:
                checked = u'checked="checked"'
            s = s + u'<input type="radio" name="{3}" id="radio-choice-{0}" value="{1}" {2}/><label for="radio-choice-{0}">{1}</label>'\
                .format(i,opt,checked,self._name)
        html = html + u'<fieldset data-role="controlgroup" name="{0}">{1}</fieldset>'.format(self._name,s)
        return html  

class ScaleEntry(BaseEntry):
    _max_score = None
    _left_label = None
    _right_label = None
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._type = TYPE_SCALE
            # get name
            for i in entry.getiterator('input'):
                self._name = i.get('name').encode('utf8')
                break
    
            # get max score
            self._max_score = 0
            for label in entry.getiterator('label'):
                if checkClass('ss-scalenumber',label.get('class')):
                    self._max_score += 1
    
            # get left and right label
            for td in entry.getiterator('td'):
                if checkClass('ss-leftlabel', td.get('class')):
                    self._left_label = td.text_content().encode('utf8')
                elif checkClass('ss-rightlabel', td.get('class')):
                    self._right_label = td.text_content().encode('utf8')
        else:
            self._max_score = utils.get_data_from_dict(js,'max_score',None)
            self._left_label = utils.get_data_from_dict(js,'left_label',None)
            self._right_label = utils.get_data_from_dict(js,'right_label',None)

    def random_answer(self,form_dict):
        form_dict[self._name] = '1'

    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['max_score'] = self._max_score
        d['left_label'] = self._left_label
        d['right_label'] = self._right_label
        return d
    
    def to_html(self):
        html = BaseEntry.to_html(self)
        s = u''
        for i in range(self._max_score):
            s = s + u'<div class="scale_btn" name={0} value={1}><p>{1}</p></div>'.format(self._name,i+1)
        html = html + u'<input type=hidden name={0} value="">'.format(self._name)
        html = html + u'{0}'.format(s)
        return html          

class GridEntry(BaseEntry):
    _score_labels = None
    _row_labels = None
    _row_ids = None
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            self._score_labels = []
            self._row_labels = []
            self._row_ids = []
            self._type = TYPE_GRID
            # get name
            for tr in entry.getiterator('tr'):
                if tr.get('class') is None:
                    continue
                for td in tr.getiterator('td'):
                    if checkClass('ss-gridrow-leftlabel',td.get('class')):
                        self._row_labels.append(td.text_content().encode('utf8'))                
                        break            
                for radio in tr.getiterator('input'):
                    self._row_ids.append(radio.get('name'))
                    break
                
            self._name = '&'.join(self._row_ids)
    
            # get score label
            for label in entry.getiterator('label'):
                if checkClass('ss-gridnumber', label.get('class')):
                    self._score_labels.append(label.text_content().encode('utf8'))   
        else:
            self._score_labels = utils.get_data_from_dict(js,'score_labels',None)
            self._row_labels = utils.get_data_from_dict(js,'row_labels',None)
            self._row_ids = self._name.split('&')

    def random_answer(self,form_dict):
        for row_id in self._row_ids:
            form_dict[row_id] = self._score_labels[0]

    def to_dict(self):
        d = BaseEntry.to_dict(self)
        d['score_labels'] = self._score_labels
        d['row_labels'] = self._row_labels
        return d
    
    def to_html(self):
        htmls = []
        htmls.append(u'<h1>{0}</h1><p>{1}</p>'.format(self._title,self._desc))
        for idx in range(len(self._row_labels)):
            htmls.append(self.generate_one_html(idx))
        return htmls
    
    def generate_one_html(self,idx):
        label = self._row_labels[idx]
        name = self._row_ids[idx]
        html = '<h1>{0}</h1>'.format(label)        
        s = ''
        for i in range(len(self._score_labels)):
            opt = self._score_labels[i]
            if opt == '__option__' or opt == '__other_option__':
                opt = 'Other'
            checked = ''
            if i == 0:
                checked = 'checked="checked"'
            s = s + '<input type="radio" name="{3}" id="radio-choice-{0}" value="{1}" {2}/><label for="radio-choice-{0}">{1}</label>'\
            .format(i,opt,checked,name)
        html = html + '<fieldset data-role="controlgroup" name="{0}">{1}</fieldset>'.format(name,s)
        return html         

class SubmitEntry(BaseEntry):
    MODE_SUBMIT,MODE_CONTINUE = range(2)
    _mode = None
    def __init__(self,entry,js=None):
        BaseEntry.__init__(self,entry,js)
        if js is None:
            for i in entry.getiterator('input'):
                if i.get('name').encode('utf8') == 'submit':
                    self._mode = self.MODE_SUBMIT
                    break
            else:
                self._mode = self.MODE_CONTINUE
        else:
            self._mode = utils.get_data_from_dict(js,'mode',None)

    def to_dict(self):
        return {'type':TYPE_SUBMIT,'mode':self._mode}
    def to_html(self):
        return u'<input type="submit" name="submit" value="Submit" >'

def check_and_get_title_and_action_url(URL):
    from pyquery import PyQuery as pq    
    d = pq(url=URL, opener=lambda url, **kw: urllib.urlopen(url).read())

    name = d('meta[itemprop=name]')
    if name:
        name = name.attr('content').encode('utf-8')
    desc = d('meta[itemprop=description]')
    if desc:
        desc = desc.attr('content').encode('utf-8')
    action_url = d('form')[0].get('action')    
    return name,action_url

