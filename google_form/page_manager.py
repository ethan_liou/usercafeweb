from entry_items import *
from pyquery import PyQuery as pq
from utils import *

class PageManager(object):
    MODE_LAST_PAGE,MODE_MID_PAGE = range(2)
    #page=None,url=None,content=None
    def __init__(self,**kwargs):
        # init
        self._page = -1        
        self._url = None
        self._action_url = None
        self._hidden_attrs = {}
        self._content = None
        self._mode = None
        self._finish = False
        
        self._question_arr = []
        self._question_dict_arr = []
        
        self._jump_page = []
        self._jump_multiple_idx = -1
        self._default_next_page = -999
        self._multiple_choices_idx = []
        self._multiple_choices_num_ans = ()
        
        if kwargs.has_key('url'):
            self._url = kwargs.get('url')
        if kwargs.has_key('page'):
            self._page = kwargs.get('page')
        if kwargs.has_key('content'):
            self._content = kwargs.get('content')
        else:
            self._content = get_content_follow_redirect(self._url)
    
    def replace_pages_to_submit(self,from_page,to_page):
        for idx in range(len(self._jump_page)):
            if self._jump_page[idx] == from_page:
                self._jump_page[idx] = to_page
        if self._default_next_page == from_page:
            self._default_next_page = to_page

    def parse_page_number(self,content):
        raise NotImplementedError("Subclass should implement it")

    def set_form_data(self,d):
        raise NotImplementedError("Subclass should implement it")        

    def to_dict(self):
        return {'questions':self._question_dict_arr,
                'mode':self._mode,
                'default_next_page':self._default_next_page,
                'jump_idx':self._jump_multiple_idx,
                'jump_page':self._jump_page}

    def test_next_page(self):
        page_dict = {}
        if len(self._multiple_choices_num_ans) == 0:
            html = self.get_next_content([])
            self.find_prev_page()
            page_num = self.parse_page_number(html)
            page_dict[page_num] = html
            if self._default_next_page == -999:
                self._default_next_page = page_num            
            return page_dict
            
        for idx in range(len(self._multiple_choices_num_ans)-1,-1,-1):
            # reverse
            ans_list = [0]*len(self._multiple_choices_num_ans)
            num_ans = self._multiple_choices_num_ans[idx]
            next_page_num = None
            for opt in range(num_ans):
                if idx != len(self._multiple_choices_num_ans)-1 and opt == 0:
                    continue
                ans_list[idx] = opt
                html = self.get_next_content(ans_list)
                self.find_prev_page()
                page_num = self.parse_page_number(html)
                print ans_list,page_num
                if page_num == self._page:
                    print html
                    return None
                if not page_num in page_dict.keys():
                    page_dict[page_num] = html
                    
                if self._default_next_page == -999:
                    self._default_next_page = page_num
                
                if next_page_num is None:
                    next_page_num = page_num
                elif next_page_num != page_num:
                    if self._jump_multiple_idx == -1:
                        # jump page multiple choices!
                        self._jump_multiple_idx = self._multiple_choices_idx[idx]
                        # record all answer before opt
                        self._jump_page = [next_page_num] * num_ans
                    self._jump_page[opt] = page_num
            if self._jump_multiple_idx != -1:
                self._default_next_page = -999
                break
            
        return page_dict

    def get_next_content(self,t):
        form_data = {'continue':'Continue'}
        form_data.update(self._hidden_attrs)        
        for question in self._question_arr:
            question.random_answer(form_data)

        for idx in range(len(self._multiple_choices_idx)):
            entry = self._question_arr[self._multiple_choices_idx[idx]]
            form_data[entry._name] = entry._options[t[idx]]
            if entry._options[t[idx]] == self.get_other_key():
                form_data[entry._name+"."+self.get_other_param()] = 'd'
        html = get_content_follow_redirect(self._action_url,urllib.urlencode(form_data))
        return html
    
    def find_prev_page(self):
        form_data = {'back':'Back'}
        form_data.update(self._hidden_attrs)
        for question in self._question_arr:
            question.random_answer(form_data)
        html = get_content_follow_redirect(self._action_url,urllib.urlencode(form_data))   
        if isinstance(self, PageManagerV1):
            return PageManagerV1(content=html)
        elif isinstance(self, PageManagerV2):
            return PageManagerV2(content=html)
        raise RuntimeError("Not support pagemanager")

    def parse_content(self,first_page):
        # if first, we should get title and desc
        d = pq(self._content)

        if first_page:
            # get name, desc
            name = d('meta[itemprop=name]')
            if len(name) != 0:
                name = name.attr('content').encode('utf-8')
            else:
                name = ''
            desc = d('meta[itemprop=description]')
            if len(desc) != 0:
                desc = desc.attr('content').encode('utf-8')
            else:
                desc = ''
        
        self.set_form_data(d)
        
        entrys = d('.ss-item')

        self._action_url = d('form')[0].get('action')        
        
        self._question_arr = []
        self._question_dict_arr = []
        for entry in entrys:
            e = EntryFactory.newInstance(entry)
            print str(e.__class__),e._title
            if isinstance(e, SubmitEntry):
                if e._mode == SubmitEntry.MODE_CONTINUE:
                    self._mode = self.MODE_MID_PAGE
                    continue
                else:
                    self._mode = self.MODE_LAST_PAGE
            elif isinstance(e, MultipleChoiceEntry):
                self._multiple_choices_idx.append(len(self._question_arr))
                self._multiple_choices_num_ans += (len(e._options),)
            self._question_arr.append(e)
            self._question_dict_arr.append(e.to_dict())
        if first_page:
            return name,desc
        
    def get_other_key(self):
        raise RuntimeError('need implement it')
    
    def get_other_param(self):
        raise RuntimeError('need implement it')
    
        
        
class PageManagerV1(PageManager):
    def get_other_key(self):
        return '__option__'
    
    def get_other_param(self):
        return 'other_option_'

    # backupCache and pageNumber
    def parse_page_number(self,content):
        d = pq(content)
        return int(d('input[name=pageNumber]')[0].get('value'))    
    
    def set_form_data(self,d):
        self._hidden_attrs = {}
        self._hidden_attrs['pageNumber'] = d('input[name=pageNumber]')[0].get('value')
        self._hidden_attrs['backupCache'] = d('input[name=backupCache]')[0].get('value').encode('utf8')
    
class PageManagerV2(PageManager):
    def get_other_key(self):
        return '__other_option__'
    
    def get_other_param(self):
        return 'other_option_response'
        
    # draftResponse and pageHistory
    def parse_page_number(self,content):
        d = pq(content)
        history_str = d('input[name=pageHistory]')[0].get('value').encode('utf8')
        num = int(history_str.split(',')[-1])
        if num < 0:
            num = 99999
        return num
    
    def set_form_data(self,d):
        self._hidden_attrs = {}
        self._hidden_attrs['pageHistory'] = d('input[name=pageHistory]')[0].get('value')
        self._hidden_attrs['draftResponse'] = d('input[name=draftResponse]')[0].get('value').encode('utf8')
