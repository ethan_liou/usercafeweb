import pymongo
import app_object
import user_object
import mongo_wrapper

surveys = {}

def add_survey(url,form_key):
    user_id = '5200ff2351faa63812004f64'
    appid = '520100d02a4f560ef3e3fe0b'
    u = user_object.User(user_id)
    a = u.find_app_by_id(appid)
    if a is not None:        
        data = a.add_google_survey(url)
        db_surveys = mongo_wrapper.get_instance().usercafe.surveys
        survey_id = db_surveys.insert(data)
        data['_id'] = survey_id
        surveys[form_key] = str(survey_id)
        return data
    return None

def get_url_by_form_key(form_key):
    docs = pymongo.Connection().user_cafe.docs
    doc = docs.find_one({'form_key':form_key})
    if doc is None:
        return None
    return doc['url']

def add_survey_id_to_data(data):
    form_key = None
    if '_form_key' in data.keys():
        form_key = data['_form_key']
    if 'form_key' in data.keys():
        form_key = data['form_key']
    if form_key is None:
        return None
    if form_key in surveys.keys():
        data['survey_id'] = surveys[form_key]
    else:
        # add survey 
        raw_input("Add "+form_key)
        url = get_url_by_form_key(form_key)
        if url is None:
            return None
        ret = add_survey(url,form_key)
        data['survey_id'] = ret['_id']
    return data

def load_surveys():
    cur = pymongo.Connection().usercafe.surveys.find()
    for c in cur:
        surveys[str(c['form_key'])] = str(c['_id'])

if __name__ == '__main__':
    load_surveys()
    print surveys
    infos = pymongo.Connection().user_cafe.userinfo
    new_info = pymongo.Connection().usercafe.answers
    cur = infos.find()
    idx = 0
    for c in cur:
        print idx
        idx += 1
        data = add_survey_id_to_data(c)
        if data is None:
            continue
        new_info.insert(data)        