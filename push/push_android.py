from gcm import GCM
import user_obj

class AndroidPushHandler(object):
    def __init__(self,api_key):
        self.gcm = GCM(api_key)

    def send_gcm_to_user(self,users,data):
        try:
            response = self.gcm.json_request(registration_ids=users, data=data)
            print response
        except Exception, e:
            print e
        
    def register_key(self,app_id,device_id,token):
        user_obj.update_user(device_id,app_id,token)

