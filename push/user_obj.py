import pymongo
import mongo_wrapper

def get_users_in_app_id(app_id,query_filter={}):
    db_push_users = mongo_wrapper.get_instance().usercafe_push.users
    cur = db_push_users.find({'app_id':app_id})
    device_ids = []
    for c in cur:
        device_ids.append(str(c['token']))
    return device_ids

def update_user(device_id,app_id,token):
    db_push_users = mongo_wrapper.get_instance().usercafe_push.users
    db_push_users.update(
        {'device_id':device_id,'device_os':0,'app_id':app_id},
        {'$set':{'token':token}},
        upsert=True)    