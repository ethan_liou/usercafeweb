#!/usr/bin/python
# -*- coding: utf8 -*-
import os
import json
import hashlib
try:
    import uwsgi
    support_uwsgi = True
except:
    support_uwsgi = False
    pass
import logging
from flask import Flask,request,Response,abort,render_template,make_response,escape,redirect,url_for,session,flash,jsonify

import memcache
from push.push_android import AndroidPushHandler
import push.user_obj as push_user_obj
import survey_object
import user_object
import auth_helper
import urllib

MEMCACHE_SERVER='usercafecache.4xykkg.cfg.usw1.cache.amazonaws.com:11211'

logger = logging.getLogger(__name__)

application = Flask(__name__)
application.secret_key = 'osadvkubweiubvuwebvuiw'
application.debug = True
application.template_folder = 'web'

ANDROID_GCM_API_KEY = 'AIzaSyDr-X2xD3oafuuv0mDKtDbL6GXS64_KzaU'
android_push_handler = AndroidPushHandler(ANDROID_GCM_API_KEY)

def get_user_id():
    return auth_helper.get_user_id(session)

@application.route('/pushapi/app/<app_id>/survey/<survey_id>/push', methods=['POST'])
def push_survey(app_id,survey_id):
    user_id = get_user_id()
    u = user_object.User(user_id) 
    a = u.find_app_by_id(app_id)
    if a is not None:
        raw_data = a.get_survey_raw_data(survey_id)
        devices = push_user_obj.get_users_in_app_id(app_id)
        data = {}
        data['title'] = raw_data['title']
        data['survey_id'] = survey_id
        data['app_id'] = app_id
        if len(devices) != 0:
            try:
                android_push_handler.send_gcm_to_user(devices,data)
            except:
                pass        
        return make_response("",200)                        
    return make_response("",400)

@application.route('/pushapi/app/<app_id>/device/<device_id>/register',methods=['POST'])
def register_key(app_id,device_id):
    data = request.form.to_dict()
    if 'device_os' in data.keys() and 'token' in data.keys():
        try:
            if int(data['device_os']) == 0:
                android_push_handler.register_key(app_id,device_id,data['token'])
            return make_response("",200)                
        except:
            pass
    return make_response("",400)

if __name__ == '__main__':
    application.run(host="0.0.0.0",port=8080)