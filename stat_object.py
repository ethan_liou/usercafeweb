import pymongo
import mongo_wrapper

def add_custom_events(app_id,survey_id,key,arg,cnt):
    db_stat = mongo_wrapper.get_instance().usercafe.statistics
    db_stat.update({'app_id':app_id,'survey_id':survey_id},{'$inc':{key+'.'+arg:cnt}},upsert=True)
    