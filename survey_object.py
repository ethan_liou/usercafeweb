import pymongo
import logging
from bson.objectid import ObjectId
import utils
import answer_object
import mongo_wrapper

SURVEY_SOURCE_GOOGLE,_ = range(2)

def get_title_by_survey_id(survey_id):
    db_surveys = mongo_wrapper.get_instance().usercafe.surveys
    survey = db_surveys.find_one({'_id':ObjectId(survey_id)})
    if survey:
        return survey['title']
    return survey_id
    
def get_questions_by_survey_id(survey_id):
    arr = []
    db_surveys = mongo_wrapper.get_instance().usercafe.surveys
    qs = db_surveys.find_one({'_id':ObjectId(survey_id)})
    for v in qs['pages'].values():
        for q in v['questions']:
            if 'name' in q and q['name'].startswith('entry.'):
                q['name'] = q['name'].replace('.','_')
                arr.append(q)
    return arr

def get_survey_from_form_key(form_key):
    db_surveys = mongo_wrapper.get_instance().usercafe.surveys
    survey = db_surveys.find_one({'form_key':form_key})
    survey['_id'] = str(survey['_id'])
    return survey

def get_survey_id_from_form_key(form_key):
    db_surveys = mongo_wrapper.get_instance().usercafe.surveys
    survey = db_surveys.find_one({'form_key':form_key})
    if survey is not None:
        return str(survey['_id'])
    return None

def is_valid_survey(app_id,survey_id):
    db_surveys = mongo_wrapper.get_instance().usercafe.surveys
    survey = db_surveys.find_one({'app_id':app_id,'_id':ObjectId(survey_id)})
    return survey != None

def list_all_surveys_simple(app_id):
    import time  
    surveys = {}        
    db_surveys = mongo_wrapper.get_instance().usercafe.surveys
    cur = db_surveys.find({'app_id':app_id},fields=[
        '_id','title','update_time','url','properties'
    ])
    for survey in cur:
        survey['_id'] = str(survey['_id'])
        survey['app_id'] = app_id
        if 'update_time' not in survey.keys(): survey['update_time'] = int(time.time())
        #survey['reply'] = answer_object.get_answer_count(survey['_id'])
        survey['reply'] = 0
        surveys[survey['_id']] = Survey(survey) 
    return surveys    

def list_all_surveys(app_id):
    import time    
    surveys = {}        
    db_surveys = mongo_wrapper.get_instance().usercafe.surveys
    cur = db_surveys.find({'app_id':app_id})
    for survey in cur:
        survey['_id'] = str(survey['_id'])
        if 'update_time' not in survey.keys(): survey['update_time'] = int(time.time())
        if 'reply' not in survey.keys(): survey['reply'] = 0
        surveys[survey['_id']] = Survey(survey) 
    return surveys
    
def insert_survey(data):
    if 'app_id' in data:
        db_surveys = mongo_wrapper.get_instance().usercafe.surveys
        try:
            db_surveys.update(
                {'app_id':str(data['app_id']),'form_key':data['form_key']},
                {'$set':data},
                upsert=True)
                # update
            survey = db_surveys.find_one(
                {'app_id':str(data['app_id']),'form_key':data['form_key']})
            if survey is not None:
                survey['_id'] = str(survey['_id'])
            return Survey(survey)
        except:
            logging.error("Add survey failed")
    return None

def convert_survey_from_google(url):
    import google_form.doc_manager    
    # convert url
    mgr = google_form.doc_manager.DocManager(url)
    data = mgr.parse_doc()
    if data is not None:
        data['survey_type'] = SURVEY_SOURCE_GOOGLE
    return data

class Survey(object):
    def __init__(self,survey_data):
        self.survey_id = survey_data['_id']   
        self.survey_data = survey_data
        self.survey_raw_data = None
    
    def _load_raw_data(self):
        db_surveys = mongo_wrapper.get_instance().usercafe.surveys
        self.survey_raw_data = db_surveys.find_one({'_id':ObjectId(self.survey_id)})  
        if self.survey_raw_data is not None:
            self.survey_raw_data['_id'] = str(self.survey_raw_data['_id'])
    
    def get_raw_data(self):
        if self.survey_raw_data is None:
            print 'reload survey {0}'.format(self.survey_id)
            self._load_raw_data()        
        return self.survey_raw_data
    
    def delete_cache(self):
        del self.survey_raw_data
    
    def get_google_detail(self):
        data = {}
        data['title'] = utils.get_data_from_dict(self.survey_raw_data,'title','')
        data['desc'] = utils.get_data_from_dict(self.survey_raw_data,'desc','')
        data['questions'] = []
        pagenums = self.survey_raw_data['pages'].keys()
        pagenums.sort()
        for pagenum in pagenums:
            data['questions'] = data['questions'] + self.survey_raw_data['pages'][pagenum]['questions']
        return data
    
    def delete(self):
        if self.survey_id:
            answer_object.delete_answer(self.survey_id)
            db_surveys = mongo_wrapper.get_instance().usercafe.surveys
            db_surveys.remove({'_id':ObjectId(self.survey_id)})        
        
    def update(self,data):
        db_surveys = mongo_wrapper.get_instance().usercafe.surveys
        ret = db_surveys.update({'_id':ObjectId(self.survey_id)},{'$set':data},upsert=True)
        if ret['ok']:
            utils.recursive_update(self.survey_data,data)
            return True
        return False
