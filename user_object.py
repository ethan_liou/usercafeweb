import pymongo
import logging
from bson.objectid import ObjectId
import app_object
import mongo_wrapper

user_id_email_map = {}

def clear_cache(user_id):
    try:
        del user_id_email_map[user_id]
    except:
        pass

def load_user_into_cache():
    db_users = mongo_wrapper.get_instance().usercafe.users
    users = db_users.find()
    if users is not None:
        for user in users:
            if '_id' in user:
                user_id_email_map[str(user['_id'])] = User(str(user['_id']),user,True,False)

def add_user(data):
    if 'email' in data:
        db_users = mongo_wrapper.get_instance().usercafe.users
        try:
            user_id = db_users.insert(data)
            data['_id'] = str(user_id)
            return User(data['_id'],data,False,False)
        except:
            logging.error("Dup email {0}".format(data['email']))
    return None

def get_user_by_email(email,load_app_data=False):
    db_users = mongo_wrapper.get_instance().usercafe.users
    user_data = db_users.find_one({'email':email})
    if user_data is not None:
        return User(str(user_data['_id']), user_data)
    return None

def get_user_by_id(user_id):
    if user_id in user_id_email_map.keys():
        return user_id_email_map[user_id]
    db_users = mongo_wrapper.get_instance().usercafe.users
    user_data = db_users.find_one({'_id':ObjectId(user_id)})
    if user_data is not None:
        u = User(user_id, user_data)
        user_id_email_map[user_id] = u
        return u
    return None

class User(object):
    def __init__(self,user_id,user_data=None):
        self.user_id = user_id
        self.user_data = user_data
        if self.user_data is None:
            self._load_user_data()
        # (id,app) dict
        self._apps = None

    def apps(self):
        if self._apps is None:
            self._apps = self._load_all_apps()
        return self._apps

    def app(self,appid):
        if appid in self.apps():
            return self.apps()[appid]
        return None

    def email(self):
        return self.user_data['email']

    def get_role(self,appid):
        if appid in self.apps():
            return self.apps()[appid].app_data['self_permission']
        return None

    def _load_user_data(self):
        db_users = mongo_wrapper.get_instance().usercafe.users
        self.user_data = db_users.find_one({'_id':ObjectId(self.user_id)})        

    def find_app_by_id(self,app_id):
        if self.apps().has_key(app_id):
            return self.apps()[app_id]
        return None
            
    # apps...
    def _load_all_apps(self):
        return app_object.list_all_apps(self.email())
    
    def list_all_apps(self):
        apps = []
        for app in self.apps().values():
            apps.append(app.app_data)
        return apps
    
    def delete_app(self,app_id):
        if app_id in self.apps().keys():
            self.apps()[app_id].delete()
            del self.apps()[app_id]
            return True
        return False
        
    def add_app(self,data):
        data['user_id'] = self.user_id
        data['permission'] = {self.email().replace('.','|') : 'owner'}
        app_data = app_object.insert_app(data)
        if app_data:
            self.apps()[str(app_data['_id'])] = app_object.App(app_data)
        return app_data
        
    def update_survey_mapping(self,app_id,survey_id,user_defined_id):
        if app_id in self.apps().keys():
            return self.apps()[app_id].update_mapping(survey_id,user_defined_id)
        return False
        
    def update_permission(self,app_id,account,role):
        if app_id in self.apps().keys():
            return self.apps()[app_id].update_permission(account,role)
        return False
        
    def delete_permission(self,app_id,account):
        if app_id in self.apps().keys():
            return self.apps()[app_id].delete_permission(account)
        return False
        
    def update_app_info(self,app_id,data):
        if app_id in self.apps().keys():
            self.apps()[app_id].update(data)
            return True
        return False
