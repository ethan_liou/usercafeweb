import urllib2
import os

import logging

NGINX_CACHE_FOLDER = '/opt/nginx_cache'
GS_URL = 'https://script.google.com/macros/s/AKfycbyj-cP8-tD31eBWLLPvbEfLtLq0qRBd2sxeYlFCBvdhKMjD3OI/exec'
def clear_nginx_cache():
    try:
        if os.path.exists(NGINX_CACHE_FOLDER):
            for filename in os.listdir(NGINX_CACHE_FOLDER):
                filepath = os.path.join(NGINX_CACHE_FOLDER,filename)
                os.remove(filepath)
    except Exception, e:
        print 'purge cache failed ', e

def get_data_from_dict(d,key,def_value):
    if key in d:
        return d[key]
    else:
        return def_value

def encrypt (data, key):
    from itertools import izip, cycle
    return ''.join(chr(ord(x) ^ ord(y)) for (x,y) in izip(data, cycle(key)))

def send_request(url,method,cookie,data):
    request = urllib2.Request(url)
    cookie_str = ''
    for k,v in cookie.iteritems():
        if len(cookie_str) != 0:
            cookie_str = cookie_str + '; '
        cookie_str = cookie_str + '{0}={1}'.format(k,v)
    if len(cookie_str) != 0:
        request.add_header('cookie',cookie_str)
    if method != 'POST':
        request.get_method = lambda: method  
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
    response = opener.open(request,data)
    return response.read()    

def get_content_follow_redirect(url,data=None):
    RETRY_CNT = 5
    for i in range(RETRY_CNT):
        try:
            opener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
            response = opener.open(url,data)
            return response.read()
        except Exception, e:
            print 'get content {0},{1} error {2} {3} times...'.format(url,data,str(e),i)
    raise Exception('Get content error')

def call_gs_with_params(params):
    print params
    utf8_params = {}
    for k,v in params.iteritems():
        utf_k = k
        utf_v = v
        if isinstance(k,unicode):
            utf_k = k.encode('utf-8')
        if isinstance(v,unicode):
            utf_v = v.encode('utf-8')
        utf8_params[utf_k] = utf_v
    params = utf8_params
    import urllib
    req = urllib2.Request(GS_URL)  
    data = urllib.urlencode(params)  
    #enable cookie  
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor())  
    response = opener.open(req, data)  
    ret = response.read()
    return ret

def recursive_update(ori_data,new_data):
    for k,v in new_data.iteritems():
        if '.' in k:
            key_idx = k.index('.')
            key = k[:key_idx]
            sub_key = k[key_idx+1:]
            if key not in ori_data:
                ori_data[key] = {}
            recursive_update(ori_data[key],{sub_key:v})
        else:
            ori_data[k] = v