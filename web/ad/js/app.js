/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/8
 * Time: 下午11:22
 * To change this template use File | Settings | File Templates.
 */
var usercafe = angular.module('usercafe', ['ui.bootstrap','imageupload']);

//usercafe.run(function($httpBackend) {
//    ads = [
//        {title: 'ad1',url: '',view_cnt: 50,response_cnt:5,enabled:true},
//        {title: 'ad2',url: '',view_cnt: 500,response_cnt:50,enabled:false}
//    ];
//    $httpBackend.whenGET('/api/ads').respond(ads);
//});

function DialogController($scope, dialog){
    $scope.close = function(){
        dialog.close(null);
    };
    $scope.save = function(index) {
        dialog.close($scope.image);
    }
}



usercafe.controller( "rootController", function($scope,$window,$location,$http,$dialog,$route){
    $http.defaults.headers.post['Content-Type'] = 'application/json; charset=UTF-8';
    $scope.change_idx = [];

    var ad_app_id = '520195602a4f5641c87921ed';

    $scope.login = function(){login($window,"ad")};
    $scope.logout = function(){logout($http,$window)};

    $scope.loadResponse = function(index){
        var ad = $scope.ads[index];
        if($scope.ads[index]['response_cnt'] != 0)
            $window.open("http://usercafe.whoscall.com/response.html?app="+ad_app_id+"&survey="+$scope.ads[index]['survey_id'], "", 'width=960');
    }

    $scope.add_change_idx = function(index){
        if(!_.contains($scope.change_idx,index)){
            $scope.change_idx.push(index);
        }
    }

    $http.get('/adapi/ads').success(function(data){
        $scope.ads = data;
    });

    $scope.saveChanged = function(){
        var len = $scope.change_idx.length;
        for(var i = 0 ; i < len ; i++){
            var idx = $scope.change_idx.pop();
            var ad = $scope.ads[idx];
            var data = {}
            if(ad['banner'] != null)
                data['banner'] = ad['banner'];
            if(ad['enabled'] != null)
                data['enabled'] = ad['enabled'];
            $http.post('/adapi/ads/'+ad['survey_id'],data)
                .error(function(){
                    alert('Failed! Try later!');
                });
        }
    }

    $scope.deleteSurvey = function(index){
        var current_index = index;
        Confirm($dialog,"Delete","Are you sure to delete this survey?",function(){
            $http.delete('/adapi/ads/'+$scope.ads[current_index]['survey_id'])
                .success(function(data){
                    $scope.ads.splice(current_index, 1);
                    ShowInfo($dialog,"Delete Successfully!","");
                })
                .error(function(){
                    ShowInfo($dialog,"Delete Failed","Please contact with Ethan");
                });
        });
    }

    $scope.updateSurvey = function(index){
        var current_index = index;
        Confirm($dialog,"Re-Convert","Are you sure to re-convert from google docs?",function(){
            Loading($dialog,"Converting","Please wait...",function(dialog){
                $http.put('/adapi/ads',{'url':$scope.ads[current_index]['url']})
                    .success(function(data){
                        $http.get('/adapi/ads').success(function(data){
                            dialog.close();
                            $scope.ads = data;
                            ShowInfo($dialog,"Convert Successfully!","");
                        });
                    })
                    .error(function(){
                        dialog.close();
                        ShowInfo($dialog,"Convert Failed","Please contact with Ethan");
                    });
            });
        });
    }

    $scope.deleteBanner = function(ad,index){
        Confirm($dialog,"Delete banner","Are you sure to delete this banner?",function(){
            ad['banner'].splice(index,1);
            $scope.add_change_idx($scope.ads.indexOf(ad));
        });
    }

    $scope.uploadAd = function(){
        InputBox($dialog,"Fill Survey URL","Upload your survey url",function(res){
            Loading($dialog,"Converting","Please wait...",function(dialog){
                $http.put('/adapi/ads',{'url':res})
                    .success(function(data){
                        $http.get('/adapi/ads').success(function(data){
                            dialog.close();
                            $scope.ads = data;
                            ShowInfo($dialog,"Convert Successfully!","");
                        });
                    })
                    .error(function(){
                        dialog.close();
                        ShowInfo($dialog,"Convert Failed","Please contact with Ethan");
                    });
            });
        });
    }

    $scope.uploadPic = function(index){
        var current_index = index;
        var t = '<div class="modal-header">'+
            '<h3>Upload Banner for '+$scope.ads[index].title+'</h3>'+
            '</div>'+
            '<div class="modal-body">'+
            '<p>Select a file</p>'+
            '<input id="file" type="file" accept="image/*" image="image" resize-max-height="320" resize-max-width="50" resize-quality="0.7" />'+
            '<img ng-show="image" ng-src="{{image.dataURL}}"/>'+
            '</div>'+
            '<div class="modal-footer">'+
            '<button ng-click="save('+index+')" class="btn btn-primary" >OK</button>'+
            '<button ng-click="close()" class="btn" >Cancel</button>'+
            '</div>';

        opts = {
            backdrop: true,
            keyboard: true,
            backdropClick: true,
            template:  t,
            controller: DialogController
        };

        $dialog.dialog(opts).open().then(function(data){
            if($scope.ads[current_index].banner == null){
                $scope.ads[current_index].banner = [];
            }
            if(data != null){
                $scope.ads[current_index].banner.push(data.dataURL);
                $scope.add_change_idx(current_index);
            }
        });
    }
});
