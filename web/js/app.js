/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/8
 * Time: 下午11:22
 * To change this template use File | Settings | File Templates.
 */
var usercafe = angular.module('usercafe', ['ui.bootstrap','ngResource','pascalprecht.translate']);
usercafe.config( function ($routeProvider, $translateProvider){
    $routeProvider
        .when('/',{
            controller:'AppOverviewViewController',
            templateUrl:'app_overview.html'
        })
        .when('/myapp/:appindex',{
            controller:'AppDetailViewController',
            templateUrl:'app_detail.html'
        })
        .when('/integration',{
            templateUrl:'integration.html'
        })
        .when('/tour',{
            controller:'TourViewController',
            templateUrl:'tour.html'
        })
        .otherwise({ redirectTo:'/'});

        $translateProvider.translations('en', {
            APP_TITLE:"Apps",
            DOC_TITLE:"Integration",
            OTHER_STR: 'Other',
            COMMON_OK:'OK',
            COMMON_CANCEL:'Cancel',
            COMMON_CLOSE:'Close',
            COMMON_DELETE:'Delete',
            MENU_SETTING: 'Settings',
            MENU_LANGUAGE: 'Language',
            MENU_ENG: 'English',
            MENU_CHT: 'Chinese',
            MENU_LOGOUT: 'Logout',
            TOUR_TITLE: 'Tour',
            TOUR_STEP1_TITLE: 'User Google form to design the survey',
            TOUR_STEP1_DESC: 'You currently use Google form to design your survey and upload the url to UserCafe',
            TOUR_STEP2_TITLE: 'Upload the survey to UserCafe',
            TOUR_STEP2_DESC: 'You could upload your Google form url to User cafe, UserCafe will be depending on your App to manage your survey',
            TOUR_STEP3_TITLE: 'Embed UserCafe SDK',
            TOUR_STEP3_DESC: 'After you upload survey,download Usercake SDK and Embed into your App,.',
            TOUR_STEP4_TITLE: 'Set the survey trigger in your app',
            TOUR_STEP4_DESC: 'You should set a survey triggers at your app, according to your App type, select the most appropriate way.',
            TOUR_STEP5_TITLE: 'Get the user feedback',
            TOUR_STEP5_DESC: 'Publish your survey to get user feedback. Survey recovery time will have slightly difference based on differnent app types',
            APP_OVERVIEW_APP_NAME: 'Name',
            APP_OVERVIEW_SYSTEM: 'System',
            APP_OVERVIEW_ONGOING: 'Ongoing Surveys',
            APP_OVERVIEW_RESPONSE: 'Totally Responses',
            APP_OVERVIEW_ADD: 'Add new app',
            APP_OVERVIEW_EMPTY_DESC: 'Starting to use User Cafe to upload your first survey to get the customer segments and user feedback. To get started, click on the button',
            APP_OVERVIEW_EMPTY_ADD: 'Add new app',
            APP_OVERVIEW_EMPTY_SEE: 'or See',
            APP_ADD_NEW_APP_TITLE: 'Add the new app',
            APP_ADD_NEW_APP_DESC: 'Use the form at the right to add a new app',
            APP_ADD_NEW_APP_SAVE: 'Save',
            APP_ADD_NEW_APP_CANCEL: 'Cancel',
            APP_SIDE_SURVEY_LIST: 'Survey list',
            APP_SIDE_SETTING:'Setting',
            APP_SIDE_CREATE:'Create the survey',
            APP_DELETE_TITLE:'Notice',
            APP_DELETE_DESC:'Are you sure to delete this app?',
            NEW_SURVEY_TITLE: 'Add New Survey',
            NEW_SURVEY_PREVIEW: 'Preview survey',
            NEW_SURVEY_FINISH:' Finish',
            NEW_SURVEY_CONVERT_GOOGLE: 'Convert the survey from GoogleDrive',
            NEW_SURVEY_CONVERT: 'Convert',
            NEW_SURVEY_FILL_URL: 'Fill Survey URL',
            NEW_SURVEY_UPLOAD: 'Upload your survey url',
            NEW_SURVEY_CONVERTING: 'Converting',
            NEW_SURVEY_WAITING: 'Please wait...',
            NEW_SURVEY_PREV:'Prev step',
            NEW_SURVEY_NEXT:'Next step',
            NEW_SURVEY_SUCCESS:'Congratulation!',
            NEW_SURVEY_NAME:'Survey Name',
            NEW_SURVEY_ID:'Survey ID',
            NEW_SURVEY_BACK:'Back to survey list',
            SURVEY_TITLE:'Title',
            SURVEY_UPDATE_TIME:'Update Time',
            SURVEY_REPLY:'Reply',
            SURVEY_REPLY_ANSWER:'Answer',
            SURVEY_REPLY_ANALYSIS:'Analysis',
            SURVEY_ESCAPE_FINISHED_TITLE:'Question title',
            SURVEY_ESCAPE_TOTAL_FILLED:'Enter count : ',
            SURVEY_ESCAPE_FINISHED_CNT:'Finished count',
            SURVEY_SETTING:'Setting',
            SURVEY_SHOW:'Show',
            DOWNLOAD_APP:'You can download demo app to test this survey',
            SURVEY_DELETE:'Delete',
            SURVEY_REUPLOAD:'Reupload',
            SURVEY_DELETE_TITLE:'Delete',
            SURVEY_DELETE_DESC:'Are you sure to delete',
            SURVEY_OK:'ok',
            SURVEY_CANCEL:'cancel',
            SURVEY_REUPLOAD_TITLE:'Reupload',
            SURVEY_REUPLOAD_DESC:'Are you sure to reupload',
            SURVEY_SETTING_KEY:'Key',
            SURVEY_SETTING_REPEAT:'Repeat',
            SURVEY_SETTING_DEMOGRAPHY:'Demography',
            SURVEY_SETTING_DEVICEINFO:'Device Info Collection',
            SURVEY_SETTING_UPDATE_SUCCESS:'Update properties successfully!',
            SURVEY_SETTING_ENABLE:'Enable',
            SURVEY_SETTING_DISABLE:'Disable',
            SURVEY_SETTING_ALLOW:'Allow',
            SURVEY_SETTING_DISALLOW:'Disallow',
            SURVEY_EMPTY_STEP1:'Step1：Design survey on google form',
            SURVEY_EMPTY_STEP2:'Step2：Upload survey url to User Cafe',
            SURVEY_EMPTY_CREATE:'Create a survey',
            APP_SETTING_BASIC:'Basic',
            APP_SETTING_PERMISSION:'Permission',
            APP_SETTING:'App Key',
            APP_SETTING_APP_NAME: 'App Name',
            APP_SETTING_APP_SYSTEM: 'System',
            APP_SETTING_APP_URL: 'App Url',
            APP_SETTING_APP_CATEGORY: 'Category',
            APP_SETTING_APP_PRICE: 'Price',
            APP_SETTING_APP_FREE: 'Free',
            APP_SETTING_APP_PAID: 'Paid',
            APP_SETTING_DELETE:'Delete App',
            APP_SETTING_SAVE:'Save changes',
            APP_SETTING_PERM_ADD: 'Add new developer',
            APP_SETTING_PERM_ACC: 'Account',
            APP_SETTING_PERM_ROLE: 'Role',
            APP_SETTING_PERM_SETTING: 'Setting',
            APP_SETTING_PERM_OWNER: 'Owner',
            APP_SETTING_PERM_ADMIN: 'Admin',
            APP_SETTING_PERM_ADMIN_DESC: 'Admin can add/modify/delete surveys and modify permission settings',
            APP_SETTING_PERM_VIEWER: 'Viewer',
            APP_SETTING_PERM_VIEWER_DESC: 'Viewer can only see survey response',
            ERROR_SERVER:'Server Error',
            ERROR_TRY_LATER:'Please try later...',
            LOADING_RESPONSE_TITLE: 'Data processing',
            LOADING_RESPONSE_WAITING: 'Please wait...',
            ALIAS_TITLE:"Survey Alias",
            ALIAS_ERROR:"This alias is used by other survey, and you will overwrite it."
        });
        $translateProvider.translations('cht', {
            APP_TITLE:"Apps",
            DOC_TITLE:"整合文件",
            OTHER_STR: '其他',
            COMMON_OK:'確認',
            COMMON_CANCEL:'取消',
            COMMON_CLOSE:'關閉',
            COMMON_DELETE:'刪除',
            MENU_SETTING: '帳戶設定',
            MENU_LANGUAGE: '語言設定',
            MENU_ENG: '英文',
            MENU_CHT: '中文',
            MENU_LOGOUT: '登出',
            TOUR_TITLE: '新手上路',
            TOUR_STEP1_TITLE: '透過Google表單設計調查',
            TOUR_STEP1_DESC: 'User Cafe目前支援Google問卷的轉換，您可以利用Google 表單設計您的問卷，並上傳Url至User Cafe',
            TOUR_STEP2_TITLE: '上傳問卷到User Cafe',
            TOUR_STEP2_DESC: 'User Cafe將根據您不同App去管理您的調查，請建立您的App，並上傳您的Google 問卷Url',
            TOUR_STEP3_TITLE: '崁入User Cafe SDK',
            TOUR_STEP3_DESC: '上傳完問卷後，下載Usercafe SDK 並崁入到您App中。',
            TOUR_STEP4_TITLE: '設定如何觸發問卷',
            TOUR_STEP4_DESC: 'User Cafe提供數種讓App使用者觸發問卷的方式，根據您的App類型，選擇最適合的觸發方式。',
            TOUR_STEP5_TITLE: '即時取得使用者資訊',
            TOUR_STEP5_DESC: '發布您的問卷取得使用者的回饋。問卷回收的時間，將依據使用者進入App的頻率以及觸發的方式而有些微差異。',
            APP_OVERVIEW_APP_NAME: 'App名稱',
            APP_OVERVIEW_SYSTEM: '系統',
            APP_OVERVIEW_ONGOING: '調查數量',
            APP_OVERVIEW_RESPONSE: '調查回收數量',
            APP_OVERVIEW_ADD: '新增一個App',
            APP_OVERVIEW_EMPTY_DESC: 'User Cafe將根據您不同App去管理您的調查，請建立您的App，上傳您的Google問卷，開始體驗User Cafe',
            APP_OVERVIEW_EMPTY_ADD: '馬上新增',
            APP_OVERVIEW_EMPTY_SEE: '或 前往',
            APP_ADD_NEW_APP_TITLE: '新增一個App',
            APP_ADD_NEW_APP_DESC: '請填寫以下新增表單',
            APP_ADD_NEW_APP_SAVE: '確認',
            APP_ADD_NEW_APP_CANCEL: '取消',
            APP_SIDE_SURVEY_LIST: '調查列表',
            APP_SIDE_SETTING:'設定',
            APP_SIDE_CREATE:'建立調查',
            APP_DELETE_TITLE:'注意',
            APP_DELETE_DESC:'你確定要刪除這個APP嗎？',
            NEW_SURVEY_TITLE: '建立調查',
            NEW_SURVEY_PREVIEW: '預覽調查',
            NEW_SURVEY_FINISH:' 完成',
            NEW_SURVEY_CONVERT_GOOGLE: '上傳您的Google Form URL',
            NEW_SURVEY_CONVERT: '上傳',
            NEW_SURVEY_FILL_URL: '輸入您的Google Form URL',
            NEW_SURVEY_UPLOAD: '上傳調查',
            NEW_SURVEY_CONVERTING: '上傳中',
            NEW_SURVEY_WAITING: '請稍候...',
            NEW_SURVEY_PREV:'上一步',
            NEW_SURVEY_NEXT:'下一步',
            NEW_SURVEY_SUCCESS:'恭喜您已完成上傳',
            NEW_SURVEY_NAME:'調查名稱',
            NEW_SURVEY_ID:'調查ID',
            NEW_SURVEY_BACK:'返回調查列表',
            SURVEY_TITLE:'調查名稱',
            SURVEY_UPDATE_TIME:'更新時間',
            SURVEY_REPLY:'回應數',
            SURVEY_REPLY_ANSWER:'答案',
            SURVEY_REPLY_ANALYSIS:'分析',
            SURVEY_ESCAPE_FINISHED_TITLE:'問題',
            SURVEY_ESCAPE_TOTAL_FILLED:'進入問卷數: ',
            SURVEY_ESCAPE_FINISHED_CNT:'完成數量',
            SURVEY_SETTING:'調查設定',
            SURVEY_DELETE:'刪除調查',
            SURVEY_SHOW:'手機預覽',
            DOWNLOAD_APP:'你可以下載展示APP來測試這份問卷',
            SURVEY_REUPLOAD:'更新問卷',
            SURVEY_DELETE_TITLE:'刪除調查',
            SURVEY_DELETE_DESC:'您確定要刪除',
            SURVEY_REUPLOAD_TITLE:'重新上傳',
            SURVEY_REUPLOAD_DESC:'您確定要重新上傳',
            SURVEY_SETTING_KEY:'Key',
            SURVEY_SETTING_REPEAT:'是否允許重複填寫',
            SURVEY_SETTING_DEMOGRAPHY:'加入用戶資料問題',
            SURVEY_SETTING_DEVICEINFO:'收集手機資訊',
            SURVEY_SETTING_UPDATE_SUCCESS:'更新問卷設定成功!',
            SURVEY_SETTING_ENABLE:'開啟',
            SURVEY_SETTING_DISABLE:'關閉',
            SURVEY_SETTING_ALLOW:'允許',
            SURVEY_SETTING_DISALLOW:'不允許',
            SURVEY_EMPTY_STEP1:'Step1：先透過Google表單設計問卷',
            SURVEY_EMPTY_STEP2:'Step2：上傳問卷Url到User Cafe，開始調查',
            SURVEY_EMPTY_CREATE:'新增調查',
            APP_SETTING_BASIC:'基本設定',
            APP_SETTING_PERMISSION:'權限設定',
            APP_SETTING:'App Key',
            APP_SETTING_APP_NAME: 'App名稱',
            APP_SETTING_APP_SYSTEM: '系統',
            APP_SETTING_APP_URL: 'App Url',
            APP_SETTING_APP_CATEGORY: 'App分類',
            APP_SETTING_APP_PRICE: '付費模式',
            APP_SETTING_APP_FREE: '免費',
            APP_SETTING_APP_PAID: '付費',
            APP_SETTING_DELETE:'刪除App',
            APP_SETTING_SAVE:'儲存',
            APP_SETTING_PERM_ADD: '新增帳號',
            APP_SETTING_PERM_ACC: '帳號',
            APP_SETTING_PERM_ROLE: '角色',
            APP_SETTING_PERM_SETTING: '設定',
            APP_SETTING_PERM_OWNER: '擁有者',
            APP_SETTING_PERM_ADMIN: '管理者',
            APP_SETTING_PERM_ADMIN_DESC: '管理者可以新增/修改/刪除問卷和修正App設定',
            APP_SETTING_PERM_VIEWER: '觀看者',
            APP_SETTING_PERM_VIEWER_DESC: '觀看者只可以觀看問卷的結果',
            ERROR_SERVER:"伺服器錯誤",
            ERROR_TRY_LATER:'請稍後再試...',
            LOADING_RESPONSE_TITLE: '資料處理中',
            LOADING_RESPONSE_WAITING: '請稍候...',
            ALIAS_TITLE:"問卷別名",
            ALIAS_ERROR:"這個別名已被其他問卷使用,你會覆蓋掉原先的設定"
        });
        var language = document.cookie.replace(/(?:(?:^|.*;\s*)language\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        if(!language){
            language = "cht";
        }
        $translateProvider.preferredLanguage(language);
    }
);
usercafe.controller( "rootController", function($scope,$window,$http,$translate){
    $scope.active = 0;
    $scope.$on('$routeChangeSuccess', function (scope, next, current) {
        if(next.loadedTemplateUrl == "app_overview.html"){
            $scope.active = 0;
        }
        else if(next.loadedTemplateUrl == "app_detail.html"){
            $scope.active = 0;
        }
        else if(next.loadedTemplateUrl == "integration.html"){
            $scope.active = 1;
        }
        else if(next.loadedTemplateUrl == "tour.html"){
            $scope.active = 2;
        }
    });

    $scope.active_state = function(idx){
        if($scope.active == idx)
            return "active";
        else return "";
    }

    $scope.$on('LOAD',function(){
        $scope.loading = true;
    });
    $scope.$on('UNLOAD',function(){
        $scope.loading = false;
    });
    $scope.login = function(){login($window,"web")};
    $scope.logout = function(){logout($http,$window)};


    $scope.selectLanguage = function(lan){
        $translate.uses(lan);
        document.cookie = "language="+lan+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
    }
});
