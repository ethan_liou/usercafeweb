/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/9
 * Time: 上午7:45
 * To change this template use File | Settings | File Templates.
 */
function AddAppDialogViewController($scope, $modalInstance, $location, UserCafeService){
    $scope.app = {};
    $scope.addApp = function(){
        new UserCafeService.App($scope.app).$save(
            function(new_app_data){
                UserCafeService.apps.push(new UserCafeService.App(new_app_data));
                $modalInstance.dismiss('success');
                $location.path('/myapp/-1');
            },
            function(error){
                $modalInstance.dismiss('network issue');
            }
        );
    }
    $scope.closeDialog = function(){
        $modalInstance.dismiss('user cancel');
    };
}