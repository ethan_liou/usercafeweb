/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/9
 * Time: 上午12:21
 * To change this template use File | Settings | File Templates.
 */

var PermissionModalInstanceCtrl = function ($scope, $modalInstance, account, role) {
    $scope.add_mode = true;
    if(account){
        // add new
        $scope.add_mode = false;
        $scope.account = account;
    }
    if(role){
        $scope.role = role;
    }
    else{
        $scope.role = "admin";
    }
    $scope.ok = function (account,role) {
        $modalInstance.close(["save",account,role]);
    };

    $scope.delete = function () {
        $modalInstance.close(['delete',account]);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

function AppDetailViewController($scope,$routeParams,$location,$modal,$http,$translate,UserCafeService){
    $scope.role = "viewer";
    // app
    $scope.selectApp = function(app){
        $scope.currentApp = app;
        $scope.role = app.self_permission;

        $scope.tabs = [
            { title:$translate('APP_SIDE_SURVEY_LIST'),inactive_img:'resource/img/inner/cs_sl.png',active_img:'resource/img/inner/cs_sl_h.png' }
        ];
        if($scope.role != "viewer"){
            $scope.tabs.push({ title:$translate('APP_SIDE_SETTING'),inactive_img:'resource/img/inner/cs_setting.png',active_img:'resource/img/inner/cs_setting_h.png'});
        }

        $scope.surveys = null;
        UserCafeService.Survey.query({appId:app['_id']},
            function(surveys){
                $scope.surveys = surveys;
            }
        );
    }

    UserCafeService.loadApps().then(
        function(apps){
            $scope.apps = apps;
            var tempIndex = parseInt($routeParams.appindex);
            if(isNaN(tempIndex)){
                $scope.selectApp($scope.apps[0]);
            }
            else if(tempIndex < 0){
                $scope.selectApp($scope.apps[$scope.apps.length + tempIndex]);
            }
            else{
                $scope.selectApp($scope.apps[tempIndex]);
            }
        }
    )

    // page

    $scope.page_index = 0;

    $scope.pages = [
        {title:$translate('APP_SIDE_SURVEY_LIST'),url:'survey_list.html'},
//        {title:'Demography',url:'demography.html'},
        {title:$translate('APP_SIDE_SETTING'),url:'app_settings.html'},
        {title:$translate('APP_SIDE_CREATE'),url:'add_survey.html'},
        {title:'Survey Preview',url:'survey_preview.html'}
    ];

    $scope.reload_surveys = function(){
        UserCafeService.Survey.force_query({appId:$scope.currentApp['_id']},
            function(surveys){
                $scope.surveys = surveys;
                $scope.tabs[0].active=true;
                $scope.select_tab(0);
            }
        );
    }

    $scope.select_tab = function(page_index){
        $scope.page_index = page_index;
        if(page_index == 2){
            for(var i = 0 ; i < $scope.tabs.length ; i++){
                $scope.tabs[i].active=false;
            }
        }
        else{
        }
    };

    // function
    $scope.openPreview = function(survey){
        survey.loading = true;
        UserCafeService.Survey.get({appId:survey.app_id,surveyId:survey._id},
            function(data){
                survey.loading = false;
                $scope.questions = getQuestions(data,$translate("OTHER_STR"));
                $scope.previewItem = data;
                $scope.page_index = 3;
            },
            function(error){
                survey.loading = false;
                $scope.errorMsg = "Cannot load survey.";
            }
        );
    }

//    $scope.openPush = function(survey){
//        Confirm($modal,"Push","push immediate?",function(){
//            $http.post("/pushapi/app/"+survey['app_id']+"/survey/"+survey['_id']+"/push").success(function(){
//
//            }).error(function(){
//                    ShowInfo($dialog,"Error","Some error...");
//                });
//        });
//    }

    $scope.openAddAppDialog = function(){
        $modal.open({
            templateUrl:'add_app_dialog.html',
            controller:'AddAppDialogViewController'
        });
    };

    $scope.deleteApp = function(app){
        Confirm($modal,$translate('APP_DELETE_TITLE'),$translate('APP_DELETE_DESC'),function(){
            app.$delete(
                function(suc){
                    UserCafeService.apps.splice(UserCafeService.apps.indexOf(app), 1);
                    $location.path('/myapp');
                },
                function(err){
                    ShowInfo($modal,$translate('ERROR_SERVER'),$translate('ERROR_TRY_LATER'));
                }
            );
        });
    }

    // permission
    $scope.perm_word_mapping = {
        owner:$translate('APP_SETTING_PERM_OWNER'),
        admin:$translate('APP_SETTING_PERM_ADMIN'),
        viewer:$translate('APP_SETTING_PERM_VIEWER')
    };

    var openSetting = function(account,role){
        var modalInstance = $modal.open({
            templateUrl: 'permission_setting.html',
            controller: PermissionModalInstanceCtrl,
            resolve:{
                account : function(){ return account; },
                role : function(){ return role; }
            }
        });

        modalInstance.result.then(function (res) {
            if(res[0] == "save"){
                var d = $http.post('/api/app/'+$scope.currentApp['_id']+'/permission',{'account':res[1],'role':res[2]});
                d.then(
                    function(){
                        // succ
                        $scope.currentApp.permission[res[1]] = res[2];
                    },
                    function(){
                        // err
                        ShowInfo($modal,"Change failed","Please contact with admin");
                    }
                );

            }
            else if(res[0] == "delete"){
                var d = $http.post('/api/app/'+$scope.currentApp['_id']+'/delete_permission',{'account':res[1]});
                d.then(
                    function(){
                        // succ
                        delete $scope.currentApp.permission[res[1]];
                    },
                    function(){
                        // err
                        ShowInfo($modal,"Delete failed","Please contact with admin");
                    }
                );
            }
        });
    }

    $scope.addAccount = function(){
        openSetting(null,null);
    }

    $scope.changeSetting = function(account,role){
        openSetting(account,role);
    }

}
