/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/9
 * Time: 上午1:32
 * To change this template use File | Settings | File Templates.
 */
function AppOverviewViewController($scope,$modal,UserCafeService){
    UserCafeService.loadApps().then(
        function(app_array){
            $scope.apps = app_array;
        }
    )
    $scope.openAddAppDialog = function(){
        $modal.open({
            templateUrl:'add_app_dialog.html',
            controller:'AddAppDialogViewController'
        });
    };
}