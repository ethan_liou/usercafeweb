usercafe.controller("ChartController", function($scope){
    function drawPie(data,ele){
        d3.select("svg").remove();

        var keys = d3.keys(data);
        var values = d3.values(data);

        var width = 480,
            height = 480,
            radius = Math.min(width, height) / 2;

        var color = d3.scale.category10();

        var arc = d3.svg.arc()
            .outerRadius(radius * 0.7)
            .innerRadius(radius * 0.5);

        var svg = d3.select(ele)
            .append("svg")
            .data([values])
            .attr("width", width)
            .attr("height", height);

        // GROUP FOR CENTER TEXT
        var center_group = svg.append("svg:g")
            .attr("class", "ctrGroup")
            .attr("transform", "translate(" + (width / 2) + "," + (height / 2) + ")");

        // CENTER LABEL
        var total_count = 0;
        for(var i = 0 ; i < values.length ; i ++){
            total_count += values[i];
        }
        var pieLabel = center_group.append("svg:text")
            .attr("dy", ".35em").attr("class", "chartLabel")
            .attr("text-anchor", "middle")
            .text("Total : "+total_count);

        var arcs = svg.selectAll('g.arc')
            .data(d3.layout.pie())
            .enter()
            .append('g')
            .attr('class','arc')
            .attr("transform", "translate(" + height / 2 + "," + height / 2 + ")");

        arcs.append("path")
            .attr("d", arc)
            .style("fill", function(d,i) { return color(i); });

        function angle(d) {
            var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
            return a > 90 ? a - 180 : a;
        }

        arcs.append("text")
            .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")"; })
            .attr("dy", ".35em")
            .attr("display", function(d) { return (d.endAngle - d.startAngle)* 180 / Math.PI >= 10 ? null : "none"; })
            .style("text-anchor", "middle")
            .style("font-size", "12px")
            .text(function(d,i) { return keys[i]+"("+values[i]+")"; });
    }
    $scope.selectQuestion = function(index){
        drawPie($scope.stats[index].answer,'#chart');
    }
    $scope.selectQuestion(0);
});
