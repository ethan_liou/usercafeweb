function ColorController($scope) {
    $scope.showColor = true;
    $scope.color = {
        bg:'#e4e4e4',
        text:'#474a50',
        icon:'#198d7c',
        actionbar_bg:'#ffffff',
        actionbar_text:'#474a50',
        actionbar_underline:'#ececec',
        progress_inactive:"#c4d7d4",
        progress_active:'#198d7c',
        first_layer_bg:'#c8c8c8',
        first_layer_text:'#9d9d9d',
        second_layer_bg:'#d3d3d3',
        second_layer_text:'#838383',
        third_layer_bg:'#dadada',
        third_layer_text:'#646464',
        next_button_text:'#ffffff',
        next_button_bg:'#76b1a7',
        scale_inactive_text:'#474a50',
        scale_inactive_bg:'#e0e0e0',
        scale_active_text:'#474a50',
        scale_active_bg:'#61847d',
        button_bg:'#f0f0f0',
        button_text:'#474a50'
    };
    $scope.applyJava = function(){
        var text = document.getElementById("java").value;
        var lines = text.split('\n');
        for(var idx in lines){
            var line = lines[idx];
            var kv = line.match(/(\w+)\s*=\s*0xff(\w{6})/);
            if(kv){
                $scope.color[kv[1]] = "#"+kv[2];
            }
        }
        alert('Apply successfully!');
    }
    $scope.$watchCollection('color',function(_,_){
        // java
        $scope.javaColor =
"package PACKAGE;\n\
import com.usercafe.resource.UserCafeColor;\n\
\n\
public class CustomColor extends UserCafeColor{\n\
\tpublic CustomColor(){\n";

        for(var colorName in $scope.color) {
            $scope.javaColor += ("\t\t"+colorName+' = 0xff'+$scope.color[colorName].substring(1,7)+';\n');
        }
        $scope.javaColor += "\t}\n}";
    });
    $scope.$watch('color.icon', function(newValue, oldValue) {
        var color = newValue;
        {
            var c=document.getElementById("back");
            var ctx=c.getContext("2d");
            ctx.clearRect(0,0, c.width, c.height);
            ctx.lineWidth=4;
            ctx.lineCap='square';
            ctx.strokeStyle=color;
            ctx.moveTo(25,10);
            ctx.lineTo(17,20);
            ctx.lineTo(25,30);
            ctx.stroke();
        }

        {
            var c=document.getElementById("radio");
            var ctx=c.getContext("2d");
            ctx.clearRect(0,0, c.width, c.height);
            ctx.lineWidth=0.5;
            ctx.fillStyle=color;
            ctx.beginPath();
            ctx.arc(22,22,12,0,2*Math.PI);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(22,22,7,0,2*Math.PI);
            ctx.fill();
        }

        {
            var c=document.getElementById("checkbox");
            var ctx=c.getContext("2d");
            ctx.clearRect(0,0, c.width, c.height);
            ctx.lineWidth=1;
            ctx.strokeStyle="gray";
            ctx.strokeRect(10,10,24,24);
            ctx.strokeStyle=color;
            ctx.lineWidth=4;
            ctx.moveTo(12,20);
            ctx.lineTo(20,30);
            ctx.lineTo(34,8);
            ctx.stroke();
        }
    });
}
