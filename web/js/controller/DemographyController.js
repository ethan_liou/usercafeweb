/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/26
 * Time: 上午5:39
 * To change this template use File | Settings | File Templates.
 */
usercafe.controller('DemographyController',function($scope){
    $scope.totalNumber = 3168;
    $scope.filters = [
        {
            name:'Sex',
            data:[
                {key:"Man",number:2000},
                {key:"Woman",number:1168}
            ]
        },
        {
            name:'Age',
            data:[
                {key:'16-20',number:2000},
                {key:'21-25',number:2000},
                {key:'26-30',number:2000},
                {key:'31-35',number:2000},
                {key:'Other',number:2000}
//                {key:'41-45',value:"10%"},
//                {key:'46-50',value:"10%"}
            ]
        },
        {
            name:'Occupation',
            data:[
                {key:'Internet',number:2000},
                {key:'Marketing',number:1000},
                {key:'Sales',number:1000},
                {key:'Manufacturing',number:500},
                {key:'Other',number:1500}
            ]
        }
    ];

    for(var idx = 0 ; idx < $scope.filters.length ; idx++){
        var filter = $scope.filters[idx];
        for(var data_idx = 0 ; data_idx < filter.data.length ; data_idx++){
            var data = filter.data[data_idx];
            var percentage = Math.round( 100 * data.number / $scope.totalNumber );
            data['percentage'] = percentage.toString() + "%";
            data['chartData'] = [{value:percentage,color:"green"},{value:100-percentage,color:"gray"}];
        }
    }
//        filters:[
//            {
//                Sex: [
//                    {Sex:'Man',Number:2100,Percent:2100/totalNumber+"%"},
//                    {key:'Woman',num:1034}
//                ]
//            },
//
//        ],
//        age:[
//            {key:'16-20',num:345},
//            {key:'21-25',num:100},
//            {key:'26-30',num:100},
//            {key:'31-35',num:100},
//            {key:'36-40',num:100},
//            {key:'41-45',num:100},
//            {key:'46-50',num:100}
//        ],
//        occupation:[
//            {key:'Internet',num:100},
//            {key:'Marketing',num:200},
//            {key:'Sales',num:400},
//            {key:'Manufacturing',num:300},
//            {key:'Finance',num:100},
//            {key:'Writer',num:100},
//            {key:'Service',num:100},
//            {key:'Teachers',num:100}
//        ]
//    };
//
//    $scope.datas = [
//        [{
//            value : 70,
//            color: "green"
//        },
//        {
//            value : 30,
//            color: "gray"
//        }],
//        [{
//            value : 70,
//            color: "green"
//        },
//        {
//            value : 30,
//            color: "gray"
//        }],
//        [{
//            value : 70,
//            color: "green"
//        },
//        {
//            value : 30,
//            color: "gray"
//        }],
//        [{
//            value : 70,
//            color: "green"
//        },
//        {
//            value : 30,
//            color: "gray"
//        }],
//        [{
//            value : 70,
//            color: "green"
//        },
//        {
//            value : 30,
//            color: "gray"
//        }]
//    ];

//    $scope.data = [
//        {
//            value : 70,
//            color: "green"
//        },
//        {
//            value : 30,
//            color: "gray"
//        }
//    ];

//    $scope.options = {
//        percentageInnerCutout : 80,
//        animation: false,
//        title: "68%",
//        subtitle: "Male"
//    }
});