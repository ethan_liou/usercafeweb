/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/15
 * Time: 下午8:30
 * To change this template use File | Settings | File Templates.
 */
usercafe.controller('NewSurveyViewController',function($scope,$modal,$translate,UserCafeService){
    $scope.step = 1;

    $scope.changeStep = function(step){
        $scope.step = step;
    }
    $scope.backToList = function(){
        $scope.reload_surveys();
    }
    $scope.openConvertDialog = function(){
        InputBox($modal,$translate("NEW_SURVEY_FILL_URL"),"",function(url){
            Loading($modal,$translate('NEW_SURVEY_CONVERTING'),$translate('NEW_SURVEY_WAITING'),function(dialog){
                UserCafeService.convertGoogleDoc($scope.currentApp['_id'],url).then(
                    function(data){
                        dialog.close(true);
                        var data = data.data;
                        $scope.questions = getQuestions(data,$translate("OTHER_STR"));
                        $scope.name = data['title'];
                        $scope.survey_id = data['_id'];
                        $scope.step ++;
                    },
                    function(){
                        dialog.close(false);
                    }
                )
            });
        });
    }
});