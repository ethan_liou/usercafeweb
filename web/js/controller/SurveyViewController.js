/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/10
 * Time: 上午12:09
 * To change this template use File | Settings | File Templates.
 */

var ModalInstanceCtrl = function ($scope, $modalInstance, $translate,tmp_survey,demography,repeat, device_info, current_app) {
    var PREDEFINED_DEVICE_INFO = ['carrierName','carrierNumber','cellId','deviceModel','location','osVersion','wifiBSSID'];

    $scope.current_survey = tmp_survey;
    $scope.demography = demography;
    $scope.repeat = repeat;
    console.log(repeat);

    $scope.device_info = {}
    for(var i = 0 ; i < PREDEFINED_DEVICE_INFO.length ; i++){
        if(device_info.indexOf(PREDEFINED_DEVICE_INFO[i]) >= 0){
            $scope.device_info[PREDEFINED_DEVICE_INFO[i]] = true;
        }
        else{
            $scope.device_info[PREDEFINED_DEVICE_INFO[i]] = false;
        }
    }

    $scope.alias = "";
    $scope.original_alias = "";
    $scope.is_conflict = false;
    if(current_app.hasOwnProperty('mapping')){
        for (var name in current_app.mapping) {
            if(current_app.mapping[name] == $scope.current_survey._id){
                $scope.alias = $scope.original_alias = name;
            }
        }
    }
    $scope.checkMappingConflict = function(alias){
        $scope.mapping_changed = alias != $scope.original_alias;
        $scope.alias = alias;
        if($scope.mapping_changed && current_app.hasOwnProperty('mapping') && current_app.mapping.hasOwnProperty(alias)){
            $scope.is_conflict = true;
        }
        else{
            $scope.is_conflict = false;
        }
    }

    $scope.ok = function (demography,repeat) {
        var new_device_info = [];
        for(var key in $scope.device_info){
            if($scope.device_info[key]){
                new_device_info.push(key);
            }
        }
        if($scope.mapping_changed){
            alias = $scope.alias;
        }
        else{
            alias = null;
        }
        $modalInstance.close([demography,repeat,new_device_info,alias]);
    };

    $scope.delete = function (){
        $modalInstance.dismiss('delete');
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

usercafe.controller('SurveyViewController',function($scope,$window,UserCafeService,$modal,$translate){
    $scope.openInfo = function(survey){
        var demography = false;
        var repeat = true;
        var device_info = [];
        if(survey.hasOwnProperty('properties') && survey.properties.hasOwnProperty('demography')){
            demography = survey.properties.demography;
        }
        if(survey.hasOwnProperty('properties') && survey.properties.hasOwnProperty('repeat')){
            repeat = survey.properties.repeat;
        }
        if(survey.hasOwnProperty('properties') && survey.properties.hasOwnProperty('device_info')){
            device_info = survey.properties.device_info;
        }
        var modalInstance = $modal.open({
            templateUrl: 'setting.html',
            controller: ModalInstanceCtrl,
            resolve:{
                tmp_survey : function(){ return survey; },
                demography : function(){ return demography; },
                repeat : function() {return repeat; },
                device_info : function(){ return device_info; },
                current_app : function(){ return $scope.currentApp; }
            }
        });

        modalInstance.result.then(function (res) {
            if(!survey.hasOwnProperty("properties")){
                survey['properties'] = {};
            }
            survey.properties.demography = res[0];
            survey.properties.repeat = res[1];
            survey.properties.device_info = res[2];
            if(res[3] != null){
                UserCafeService.modifyMapping(survey.app_id,survey._id,res[3]).
                    then(
                        function(data){
                            //   succ
                            $scope.currentApp.mapping[res[3]] = survey._id;
                        }
                );
            }
            UserCafeService.updateProperties(survey.app_id,survey._id,survey.properties).
                then(
                    function(data){
                        //   succ
                        ShowInfo($modal,survey.title,$translate('SURVEY_SETTING_UPDATE_SUCCESS'));
                    }
                );
        }, function (reason) {
            // dismiss
            if(reason == "delete"){
                $scope.deleteSurvey(survey);
            }
        });
    }

    $scope.predicate = 'update_time';
    $scope.reverse = true;
    $scope.sortByKey = function(key){
        if($scope.predicate == key){
            $scope.reverse = !$scope.reverse;
        }
        else{
            $scope.predicate = key;
            $scope.reverse = true;
        }
    }

    $scope.deleteSurvey = function(survey){
        Confirm($modal,$translate('SURVEY_DELETE_TITLE'),$translate('SURVEY_DELETE_DESC')+' '+survey.title,function(){
            survey.$delete(
                function(){
                    var idx = $scope.surveys.indexOf(survey);
                    $scope.surveys.splice(idx,1);
                }
            );
        })
    }

    $scope.showSurvey = function(survey){
        $modal.open({
            templateUrl: 'qrcode.html',
            controller: function($scope,$modalInstance,text,title){
                $scope.title = title;
                $scope.text = text;
                $scope.ok = function(){
                    $modalInstance.dismiss('cancel')
                }
            },
            resolve:{
                text : function(){ return survey.app_id+","+survey._id; },
                title : function(){ return survey.title; }
            }
        });
    }

    $scope.openEscape = function(survey){
        Loading($modal,$translate('LOADING_RESPONSE_TITLE'),$translate('LOADING_RESPONSE_WAITING'),function(dialog){
            UserCafeService.getEscapeStat(survey.app_id,survey._id).then(
                function(data){
                    dialog.close(true);
                    $modal.open({
                        templateUrl: 'escape.html',
                        controller: function($scope,$modalInstance,escapes,title){
                            $scope.title = title;
                            $scope.escapes = escapes.questions;
                            $scope.total = escapes.total;
                            $scope.ok = function(){
                                $modalInstance.dismiss('cancel')
                            }
                        },
                        resolve:{
                            escapes : function(){ return data.data; },
                            title : function(){ return survey.title; }
                        }
                    });
                },
                function(){
                    dialog.close(false);
                }
            );
        });
    }

    $scope.loadResponse = function(survey){
        Loading($modal,$translate('LOADING_RESPONSE_TITLE'),$translate('LOADING_RESPONSE_WAITING'),function(dialog){
            UserCafeService.getResponseLink(survey.app_id,survey._id).then(
                function(data){
                    dialog.close(true);
                    $window.open(data.data);
                },
                function(){
                    dialog.close(false);
                }
            );
        });
    }

    $scope.reloadSurvey = function(survey){
        Confirm($modal,$translate('SURVEY_REUPLOAD_TITLE'),$translate('SURVEY_REUPLOAD_DESC')+" "+survey.title,function(){
            Loading($modal,$translate('NEW_SURVEY_CONVERTING'),$translate('NEW_SURVEY_WAITING'),function(dialog){
                UserCafeService.convertGoogleDoc(survey['app_id'],survey['url']).then(
                    function(data){
                        survey.title = data.data.title;
                        survey.update_time = data.data.update_time;
                        dialog.close();
                    }
                )
            });
        })
    }
});