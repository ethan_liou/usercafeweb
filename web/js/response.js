/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/8/10
 * Time: 上午11:22
 * To change this template use File | Settings | File Templates.
 */
var usercafe = angular.module('usercafe', ['ui.bootstrap']);

usercafe.controller("responseController", function($scope,$http,$location){
    var qs = $location.absUrl().split("?");
    qs = qs[qs.length - 1];
    var regex = /([^&=]+)=([^&]*)/g;
    var m;
    while (m = regex.exec(qs)) {
        if(m[1] == 'survey')
            $scope.survey_id = m[2];
        if(m[1] == 'app')
            $scope.app_id = m[2];
    }

    $scope.titles = []
    $scope.answers = []
    if($scope.survey_id){
        $http.get('/api/app/'+$scope.app_id+'/survey/'+$scope.survey_id+'/answers/200',{ cache: true})
            .success(function(title_answer_js){
            $scope.answers = title_answer_js['answers'];
            $scope.stats = title_answer_js['stats'];
            var tmp_titles = title_answer_js['titles'];
            $scope.titles = [{
                field:'_timestamp',
                displayName: 'Timestamp'
            }];
            // answer
            for (var idx in tmp_titles){
                if(tmp_titles[idx].field.lastIndexOf('entry_',0) === 0 &&
                    tmp_titles[idx].field.indexOf('_t',tmp_titles[idx].field.length - 2) === -1){
                    $scope.titles.push(tmp_titles[idx]);
                }
                else if(tmp_titles[idx].field != '_timestamp' &&
                    tmp_titles[idx].field != '_finished' &&
                    tmp_titles[idx].field != '_startpage' &&
                    tmp_titles[idx].field.indexOf('_t',tmp_titles[idx].field.length - 2) === -1){
                    $scope.titles.push(tmp_titles[idx]);
                }
            }

            $scope.sortByKey = function(key){
                if($scope.predicate == key){
                    $scope.reverse = !$scope.reverse;
                }
                else{
                    $scope.predicate = key;
                    $scope.reverse = false;
                }
            }
            $scope.tableStyle = {
                width: parseInt($scope.titles.length * 150) + "px",
                tableLayout: 'fixed'
            }
            $scope.predicate = '_timestamp';
            $scope.reverse = false;
            $scope.selection = 'table';
        });
    }
});