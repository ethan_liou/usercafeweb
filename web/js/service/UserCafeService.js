/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/9
 * Time: 上午1:28
 * To change this template use File | Settings | File Templates.
 */
usercafe.service('UserCafeService', function($resource,$http,$q){
     var backend =
     {
        apps : null,
        App : $resource('/api/app/:appId', {appId:'@_id'},{'query':{method:'GET', isArray:true, cache:true}}),
        Survey : $resource('/api/app/:appId/survey/:surveyId', {appId:'@app_id',surveyId:'@_id'},{'query':{method:'GET', isArray:true, cache:true},
                'force_query':{method:'GET', isArray:true, cache:false}}),
        loadApps : function(){
            var deferred = $q.defer();
            if(backend.apps != null){
                deferred.resolve(backend.apps);
            }
            else{
                backend.App.query(function(app_array){
                    backend.apps = app_array;
                    deferred.resolve(backend.apps);
                });
            }
            return deferred.promise;
        },
        convertGoogleDoc : function(app_id,url){
            return $http.post('/api/app/'+app_id+'/survey/googledoc', {'url':url});
        },
        updateProperties : function(app_id,survey_id,properties){
            return $http.post('/api/app/'+app_id+'/survey/'+survey_id+'/properties', properties);
        },
        modifyMapping : function(app_id,survey_id,alias){
            return $http.post('/api/app/'+app_id+'/modify_mapping', {'survey_id':survey_id,'user_defined_id':alias});
        },
        getResponseLink : function(app_id,survey_id){
            return $http.get('/api/app/'+app_id+'/survey/'+survey_id+'/google_drive');
        },
         getEscapeStat : function(app_id,survey_id){
             return $http.get('/api/app/'+app_id+'/survey/'+survey_id+'/escape');
         }
    }
    return backend;
//    this.App = $resource('/api/app/:appId', {appId:'@_id'});
//
//    this.apps = this.App.query();
//    this.loadApps = function(){
//
//    }
//
//    this.Survey = $resource('/api/app/:appId/survey/:surveyId', {appId:'@app_id',surveyId:'@_id'});
//
//    this.convertGoogleDoc = function(app,url){
//        return $http.post('/api/app/'+app['_id']+'/survey/googledoc', {'url':url});
//    }
});