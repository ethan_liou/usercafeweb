/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/7/15
 * Time: 下午7:55
 * To change this template use File | Settings | File Templates.
 */

var commonOpts = {
    backdrop: true,
    keyboard: true
}

var opts = {}

function Confirm(modal,title,msg,okCallback){
    var t = '<div class="modal-header">'+
        '<h3>'+title+'</h3>'+
        '</div>'+
        '<div class="modal-body">'+
        '<p>'+msg+'</p>'+
        '</div>'+
        '<div class="modal-footer">'+
        '<button ng-click="close(false)" class="btn" translate="COMMON_CANCEL"></button>'+
        '<button ng-click="close(true)" class="btn btn-primary" translate="COMMON_OK"></button>'+
        '</div>';

    angular.extend(opts,
        {
            template:  t,
            controller: function($scope, $modalInstance){
                $scope.close = function(result){
                    $modalInstance.dismiss('close');
                    if(result){
                        okCallback();
                    }
                };
            }
        },
        commonOpts);

    modal.open(opts);
}

function ShowInfo(modal,title,msg){
    var t = '<div class="modal-header">'+
        '<h3>'+title+'</h3>'+
        '</div>'+
        '<div class="modal-body">'+
        '<p>'+msg+'</p>'+
        '</div>'+
        '<div class="modal-footer">'+
        '<button ng-click="close()" class="btn" translate="COMMON_CLOSE"></button>'+
        '</div>';

    angular.extend(opts,
        {
            template:  t,
            controller: function($scope, $modalInstance){
                $scope.close = function(){
                    $modalInstance.dismiss("User stop");
                };
            }
        },
        commonOpts);

    modal.open(opts);
}

function Loading(modal,title,msg,doBackend){
    var t = '<div class="modal-header">'+
        '<h3>'+title+'</h3>'+
        '</div>'+
        '<div class="modal-body">'+
        '<div class="progress progress-striped active">'+
            '<div class="bar" style="width: 100%;"></div>'+
        '</div>'+
        '<p>'+msg+'</p>'+
        '</div>';

    angular.extend(opts,
        {
            template:  t,
            controller: function($scope, $modalInstance){
                var res = doBackend($modalInstance);
            }
        },
        commonOpts);
    opts.keyboard = false;
    opts.backdrop = 'static';

    modal.open(opts);
}

function InputBox(modal,title,msg,okCallback){
    var t = '<div class="modal-header">'+
        '<h3>'+title+'</h3>'+
        '</div>'+
        '<div class="modal-body">'+
        '<p>'+msg+'</p>'+
        '<input class="input-block-level" type="text" ng-model="text">'+
        '</div>'+
        '<div class="modal-footer">'+
        '<button ng-click="close()" class="btn" translate="COMMON_CANCEL"></button>'+
        '<button ng-click="close(text)" class="btn btn-primary" translate="COMMON_OK"></button>'+
        '</div>';

    angular.extend(opts,
        {
            template:  t,
            controller: function($scope, $modalInstance){
                $scope.close = function(text){
                    if(text != null && text != ""){
                        $modalInstance.dismiss("Success");
                        okCallback(text);
                    }
                    else{
                        $modalInstance.dismiss("User stop");
                    }
                };
            }
        },
        commonOpts);

    modal.open(opts);
}