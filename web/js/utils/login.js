/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/8/10
 * Time: 下午1:31
 * To change this template use File | Settings | File Templates.
 */
function logout(http,window){
    http.get('/api/logout').success(function(){
        window.location.reload();
    });
}

function login(window,redirect_type){
    var oauth2 = {
        url: "https://accounts.google.com/o/oauth2/auth",
        client_id: "881232093220.apps.googleusercontent.com",
        response_type: "token",
        redirect_uri: "http://usercafe.whoscall.com/api/auth/"+redirect_type,
        scope: "https://www.googleapis.com/auth/userinfo.email",
        state: "initial"
    };
    window.open(oauth2.url + "?client_id=" +
        oauth2.client_id + "&response_type=" +
        oauth2.response_type + "&redirect_uri=" +
        oauth2.redirect_uri + "&scope=" +
        oauth2.scope + "&state=" +
        oauth2.state
        ,"_self");
}