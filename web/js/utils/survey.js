/**
 * Created with JetBrains WebStorm.
 * User: Lycca
 * Date: 13/8/18
 * Time: 上午10:44
 * To change this template use File | Settings | File Templates.
 */
function getQuestions(json,otherStr){
    var keys = []
    for (var k in json.pages)
    {
        if (json.pages.hasOwnProperty(k))
        {
            keys.push(k);
        }
    }
    keys.sort();

    var len = keys.length;
    var questions = [];
    for(var i = 0 ; i < len ; i++){
        for(var question_idx in json.pages[keys[i]].questions){
            var question = json.pages[keys[i]].questions[question_idx];
            if(question.type == 2 || question.type == 3 || question.type == 4){
                for(var option_idx in question.options){
                    var option = question.options[option_idx];
                    if(option == "__other_option__" || option == "__option__"){
                        json.pages[keys[i]].questions[question_idx].options[option_idx] = otherStr;
                    }
                }
            }
        }
        questions.push.apply(questions,json.pages[keys[i]].questions);
    }
    return questions;
}