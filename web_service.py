#!/usr/bin/python
# -*- coding: utf8 -*-
import os
import json
import hashlib
import copy
try:
    import uwsgi
    support_uwsgi = True
except:
    support_uwsgi = False
    pass
import logging
from flask import Flask,request,Response,abort,render_template,make_response,escape,redirect,url_for,session,flash,jsonify

#import memcache
import user_object
import survey_object
import answer_object
import auth_helper
import stat_object
import app_object
import constant
import ad.developer_obj as dev_obj
import google_drive
import memcache
from blueprint.app_bp import app_bp
from blueprint.survey_bp import survey_bp
from blueprint.private_bp import private_bp

MEMCACHE_SERVER='usercafecache.4xykkg.cfg.usw1.cache.amazonaws.com:11211'

logger = logging.getLogger(__name__)

application = Flask(__name__)
application.secret_key = 'osadvkubweiubvuwebvuiw'
application.debug = True
application.template_folder = 'web'
application.static_folder = 'web'

application.register_blueprint(app_bp,url_prefix="/api/app")
application.register_blueprint(survey_bp,url_prefix="/api/app/<appid>/survey")
application.register_blueprint(private_bp,url_prefix="/api/private")

AD_APP_ID = '520195602a4f5641c87921ed'
DEFAULT_DEV_ID = '5201222951faa63812006f4b'

############################################################

def get_user_id():
    return auth_helper.get_user_id(session)

@application.route('/<path:p>')
def static(p):
    r = application.send_static_file(p)
    r.headers['Cache-Control'] = 'no-cache, no-store, max-age=0, must-revalidate' 
    r.headers['Expires'] = 'Fri, 01 Jan 2010 00:00:00 GMT'    
    return r

@application.route('/peter_test.html')
def index_peter():
    session['s_email'] = 'petersu@gogolook.com.tw'
    session['s_id'] = '5200ff2351faa63812004f64'
    user = auth_helper.get_user(session)
    if user is None:
        return render_template('index.html')
    else:
        return render_template('index.html',email=user.user_data['email'])

@application.route('/pp.html')
@application.route('/beta.html')
def index():
    user = auth_helper.get_user(session)
    if user is None:
        return render_template('landing.html')
    else:
        return render_template('index.html',email=user.user_data['email'])

# login,out
@application.route('/api/auth/<redirect_type>')
def auth(redirect_type):
    return '''  <script type="text/javascript">
                var url = window.location.href.replace('#','?'); 
                url = url.replace('/api/auth/','/api/login/');
                window.location = url;
            </script> '''

@application.route('/api/login/<redirect_type>')
def login(redirect_type):
    if redirect_type == 'ad':
        redirect_page = '/ad/'
    elif redirect_type == 'web':
        redirect_page = url_for('index')
    else:
        make_response("Invalid param",400)
    # ad or web
    token = request.args.get('access_token',None)
    if token is not None:
        if auth_helper.login(token,session) is not None:
            return redirect(redirect_page)
    return make_response("Auth Failed!",401)

@application.route('/api/logout')
def logout():
    auth_helper.logout(session)
    return make_response("",200)    

@application.route('/api/app/<appid>/survey/<surveyid>/get_survey',methods=['POST'])
def get_survey_raw_data(appid,surveyid):
    a = app_object.find_app_by_id(appid)
    d = request.form.to_dict()
    err = 'Not a valid app'
    if a is not None and d is not None and '_device_id' in d:
        device_id = d['_device_id']
        data = a.get_survey_raw_data(surveyid)
        err = 'not a valid survey'
        if data is not None and 'properties' in data and 'demography' in data['properties'] and (data['properties']['demography'] == "1" or data['properties']['demography'] == True):
            # insert demography
            import json
            data = copy.deepcopy(data)
            arr = json.loads(constant.DEMOGRAPHY_CHINESE_JSON)
            for val in data['pages'].values():
                if val['mode'] == 0:
                    for question in val['questions']:
                        if question['type'] == 9:
                            # reverse order
                            for demo_q in arr:
                                val['questions'].insert(val['questions'].index(question),demo_q)
                            break
            if data is not None and appid == AD_APP_ID:
                dev_obj.add_click_cnt(DEFAULT_DEV_ID)
        if data is not None and 'properties' in data and 'repeat' in data['properties'] and not data['properties']['repeat'] and answer_object.check_repeat(surveyid,device_id):
            return make_response("",204)
        if data is not None:
            return jsonify(**data)
    return make_response(err,400)

@application.route('/api/app/<appid>/survey/<surveyid>',methods=['GET'])
def old_get_survey_raw_data(appid,surveyid):
    a = app_object.find_app_by_id(appid)
    if a is not None:
        data = a.get_survey_raw_data(surveyid)
        if data is not None and 'properties' in data and 'demography' in data['properties'] and (data['properties']['demography'] == True or data['properties']['demography'] == '1'):
            # insert demography
            data = copy.deepcopy(data)
            import json
            arr = json.loads(constant.DEMOGRAPHY_CHINESE_JSON)
            for val in data['pages'].values():
                if val['mode'] == 0:
                    for question in val['questions']:
                        if question['type'] == 9:
                            # reverse order
                            for demo_q in arr:
                                val['questions'].insert(val['questions'].index(question),demo_q)
                            break
            if data is not None and appid == AD_APP_ID:
                dev_obj.add_click_cnt(DEFAULT_DEV_ID)
        if data is not None:
            return jsonify(**data)
    return make_response("",400)

@application.route('/api/app/<appid>/survey/<surveyid>',methods=['DELETE'])
def delete_survey(appid,surveyid):
    a = None
    if appid == AD_APP_ID and auth_helper.is_ad_valid_session(session):
        a = app_object.find_app_by_id(appid)
    else:
        user_id = get_user_id()
        a = None
        if user_id is not None:            
            u = user_object.get_user_by_id(user_id) 
            if not app_object.check_write_permission(u,appid):
                return make_response("No permission", 403)            
            a = app_object.find_app_by_id(appid)
    if a is not None:
        a.delete_survey(surveyid)
        app_object.clear_cache(appid)        
        return make_response("OK",200)
    return make_response("",400) 

@application.route('/api/app/<appid>/survey/<surveyid>',methods=['POST'])
def upload_answer(appid,surveyid):
    data = request.form.to_dict()
    data['_ip'] = request.remote_addr
    if appid == AD_APP_ID or survey_object.is_valid_survey(appid,surveyid):
        if answer_object.insert_answer_data(data):
            if '_device_id' in data.keys():
                mc = memcache.Client([MEMCACHE_SERVER],debug=0)
                mc.delete('user_'+str(data['_device_id']))
            if appid == AD_APP_ID and '_finished' in data and data['_finished'] == 'true':
                # workaround
                dev_obj.add_response_cnt(DEFAULT_DEV_ID)
            answer_object.modify_answer_record(surveyid,False)
            return make_response("",200)
    return make_response("",400)

@application.route('/api/app/<appid>/survey/<surveyid>/escape',methods=['GET'])
def escape_analyze(appid,surveyid):
    user_id = get_user_id()    
    if user_id is not None:
        u = user_object.get_user_by_id(user_id)
        a = u.find_app_by_id(appid)
        if a is not None:
            questions,total_cnt = a.get_answer_escape(surveyid)
            js = {'questions':questions,'total':total_cnt}
            return jsonify(**js) 
        else:
            return make_response("",500)
    return make_response("",400) 

#stat
@application.route('/api/app/<app_id>/survey/<survey_id>/stat',methods=['POST'])
def upload_stat(app_id,survey_id):
    data = request.form.to_dict()
    if 'key' in data and 'arg' in data and 'cnt' in data and data['cnt'].isdigit():
        stat_object.add_custom_events(app_id,survey_id,data['key'],data['arg'],int(data['cnt']))
        return make_response("",200)
    return make_response("",400)

#answer
@application.route('/api/app/<app_id>/survey/<survey_id>/google_drive',methods=['GET'])
def get_google_drive(app_id,survey_id):
    a = None
    user_id = get_user_id()
    if user_id is not None:
        u = user_object.get_user_by_id(user_id)
        print user_id,u
        a = u.find_app_by_id(app_id)    
    if a is not None:
        link = answer_object.get_link_if_unmodified(survey_id)
        if link:
            return make_response(link,200)
        else:
            titles = answer_object.get_answer_titles(survey_id)    
            answers = answer_object.list_answer(survey_id)
            try:
                path = "/opt/csv_data/{0}_{1}.csv".format(app_id,survey_id)
                if answer_object.generate_csv(titles,answers,path):
                    file_id,link = google_drive.upload_file(
                        survey_object.get_title_by_survey_id(survey_id),
                        path,
                        u.email(),
                        a.app_data['permission'],
                        answer_object.get_fileid(survey_id) 
                    )
                    answer_object.modify_answer_record(survey_id,True,file_id,link)
                return make_response(link,200)
            except Exception as e:
                print e
    return make_response("",400)

if __name__ == '__main__':
    application.run(host="0.0.0.0",port=8080)
